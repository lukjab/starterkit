#Migracja bazy danych z użyciem mavena

mvn flyway:clean flyway:migrate

#Instalacja postgresa w kontenerze dockera
Czyszczenie ubuntu z dockera:
https://askubuntu.com/questions/935569/how-to-completely-uninstall-docker

Instalacja dockera na ubuntu:
https://linuxize.com/post/how-to-install-and-use-docker-on-ubuntu-18-04/

#Komendy dockera w celu uruchomienia bazy w kontenerze:
docker pull postgres

docker run -it --rm --cpus 2 -p 5432:5432 -e POSTGRES_PASSWORD='test' -e POSTGRES_USER='postgres' postgres:9.6