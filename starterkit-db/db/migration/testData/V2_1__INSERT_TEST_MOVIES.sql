INSERT INTO movies(title, director, premier_date, description)
	VALUES 
	('Fight Club', 'David Fincher', '1999-09-10', 'First rule of fight club is that we dont talk about fight club.'),
	('The Game', 'David Fincher', '1997-09-12', 'On the 48th birthday Nicholas Van Orton is invited to a mysterious game that changes his monotonous life.'),
	('Se7ev', 'David Fincher', '1997-09-22', 'Two policemen try to catch a serial killer choosing their victims according to a special "key".'),
	('Reservoir Dogs', 'Quentin Tarantino', '1992-01-21', 'After a failed robbery at a jeweler''s shop a few thieves realize that there is a traitor among them who told the police about their plan.'),
	('Pulp Fiction', 'Quentin Tarantino', '1994-05-12', 'Violence and redemption in the story of two paid murderers working for the mafia, gangster wife, boxer and stealman stealing from the restaurant.');

