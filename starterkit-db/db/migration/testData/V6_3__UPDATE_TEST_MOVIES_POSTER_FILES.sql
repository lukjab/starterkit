UPDATE movies
SET poster_file = 'pulp-fiction.jpg'
WHERE title = 'Pulp Fiction';

UPDATE movies
SET poster_file = 'a-nightmare-on-elm-street.jpg'
WHERE title = 'Koszmar z ulicy Wiązów';

UPDATE movies
SET poster_file = 'a-nightmare-on-elm-street-part2.jpg'
WHERE title = 'Koszmar z ulicy Wiązów 2: Zemsta Freddy''ego';

UPDATE movies
SET poster_file = 'a-nightmare-on-elm-street-part3.jpg'
WHERE title = 'Koszmar z ulicy Wiązów 3: Wojownicy snów';

UPDATE movies
SET poster_file = 'fight-club.jpg'
WHERE title = 'Fight Club';

UPDATE movies
SET poster_file = 'the-game.jpg'
WHERE title = 'The Game';

UPDATE movies
SET poster_file = 'se7ev.jpg'
WHERE title = 'Se7ev';

UPDATE movies
SET poster_file = 'reservoir-dogs.jpg'
WHERE title = 'Reservoir Dogs';