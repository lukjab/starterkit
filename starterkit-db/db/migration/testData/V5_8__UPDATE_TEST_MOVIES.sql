UPDATE movies
SET description = 'Pierwsza zasada klubu walki – nie rozmawiajcie o klubie walki. Druga zasada klubu walki – nie rozmawiajcie o klubie walki. Trzecia zasada klubu walki – kiedy ktoś mówi „stop” albo puszcza farbę, koniec walki. Czwarta zasada klubu walki – walczy tylko dwóch facetów. Piąta zasada klubu walki – na raz może trwać tylko jedna walka. Szósta zasada – bez koszulek, bez butów. Siódma zasada – walki trwają tak długo, ile potrzeba. I ósma i ostatnia zasada – jeśli to jest twoja pierwsza noc w klubie walki, musisz walczyć.'
WHERE title = 'Fight Club';

UPDATE movies
SET description = 'W dniu 48. urodzin Nicholas Van Orton zostaje zaproszony do tajemniczej gry, która odmienia jego monotonne życie.'
WHERE title = 'The Game';

UPDATE movies
SET description = 'Siedem. To liczba ofiar, których dosięgnie gniew jego wybrańca. To liczba sposobów odebrania życia, które on wybierze. To liczba dni tygodnia z życia deszczowej metropolii, które obejrzysz na ekranie, poznając zakamarki umysłu szaleńca. Ale czy na pewno jest on szalony, czy może szalone są tylko drogi, którymi prowadzi pozbawione empatii społeczeństwo do oświecenia?'
WHERE title = 'Se7ev';

UPDATE movies
SET description = 'Po nieudanym napadzie na sklep jubilerski kilku jego uczestników spotyka się w opuszczonym magazynie, by ustalić, kto zdradził policji ich plan. Wściekłe psy to nie znający się wzajemnie, najemni gangsterzy o "kolorowych" pseudonimach. Ich zleceniodawcami są Joe Cabot i jego syn Eddie.'
WHERE title = 'Reservoir Dogs';

UPDATE movies
SET description = 'Przemoc i odkupienie w opowieści o dwóch płatnych mordercach pracujących na zlecenie mafii, żonie gangstera, bokserze i parze okradającej ludzi w restauracji.'
WHERE title = 'Pulp Fiction';

