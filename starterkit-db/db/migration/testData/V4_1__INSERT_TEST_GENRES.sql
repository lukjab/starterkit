INSERT INTO genres (name, description)
VALUES ('Horror', 'Odmiana fantastyki polegająca na budowaniu świata aby wprowadzić zjawiska kwestionujące, nie dające się wytłumaczyć bez odwoływania się do zjawisk nadprzyrodzonych'),
  ('Sci-Fi', 'Gatunek o fabule osnutej na przewidywanych osiągnięciach nauki i techniki oraz ukazującej ich wpływ na życie jednostki lub społeczeństwa'),
  ('Akcja', 'Głównym zadaniem jest dostarczanie rozrywki widzom poprzez pokazywanie pościgów samochodowych, strzelanin, bijatyk i innych scen kaskaderskich o dużym ładunku napięcia i emocji'),
  ('Dramat', 'Gatunek filmowy o sensacyjnej fabule, nasyconej patetyczno-sentymentalnymi efektami oraz wątkiem miłosnym'),
  ('Thriller', 'Rodzaj filmu mający wywołać u widza dreszcz emocji. W odróżnieniu od horroru tajemniczość i niesamowitość w thrillerze nie mają jednak cech nadprzyrodzonych, zagrożenie jest realne'),
  ('Komedia', 'Film przedstawiający sytuacje i postacie wywołujące u widzów efekt komiczny'),
  ('Pornografia', ' Wizerunek osób lub przedmiotów o cechach jednoznacznie seksualnych utworzony z zamiarem wywołania pobudzenia seksualnego lub podniecenia u osób oglądających');