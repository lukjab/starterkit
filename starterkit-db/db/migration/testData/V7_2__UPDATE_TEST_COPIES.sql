UPDATE copies
SET description = 'Kopia zniszczona przez użytkownika'
WHERE status = 'B';

UPDATE copies
SET description = 'Kopia nie została zwrócona przez użytkownika'
WHERE status = 'M';