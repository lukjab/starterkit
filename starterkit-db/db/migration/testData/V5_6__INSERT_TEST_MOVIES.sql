INSERT INTO movies(title, director, premier_date, description)
	VALUES 
	('Koszmar z ulicy Wiązów', 'Wes Craven', '1984-11-09', 'Grupę nastolatków męczą senne koszmary, w których prześladuje ich mężczyzna ze spaloną twarzą, Freddy Krueger, psychopatyczny morderca dzieci.'),
	('Koszmar z ulicy Wiązów 2: Zemsta Freddy''ego', 'Jack Sholder', '1985-11-01', 'Freddy Krueger powraca po latach, aby wybrać swojego następcę.'),
	('Koszmar z ulicy Wiązów 3: Wojownicy snów', 'Chuck Russell', '1987-02-27', 'By pokonać Freddy''ego nastolatki muszą posiąść umiejętność kontroli własnych snów.')
	