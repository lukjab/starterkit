create table MOVIES (
	id BIGSERIAL NOT NULL,
	title TEXT default NULL,
	director TEXT default NULL,
	premier_date date default NULL,
	description TEXT default NULL,
	
	PRIMARY KEY(id)
);
