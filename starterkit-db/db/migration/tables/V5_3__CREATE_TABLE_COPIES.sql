create table COPIES (
  id BIGSERIAL NOT NULL,
  fk_mov BIGINT REFERENCES MOVIES(id),
  status CHAR DEFAULT NULL,

  PRIMARY KEY (id)
);