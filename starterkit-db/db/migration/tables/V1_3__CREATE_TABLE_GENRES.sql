create table GENRES (
	id BIGSERIAL NOT NULL,
	name TEXT default NULL,
	description TEXT default NULL,
	
	PRIMARY KEY(id)
);
