create table USERS (
   id BIGSERIAL NOT NULL,
   login	  VARCHAR(20) default NULL,
   password   VARCHAR(20) default NULL,
   email      VARCHAR(40) default NULL,
   date_of_birth      DATE default NULL,
   gender     CHAR,
      
   PRIMARY KEY (id)
);
