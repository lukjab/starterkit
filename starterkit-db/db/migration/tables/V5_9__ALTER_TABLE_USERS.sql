ALTER TABLE users
	ADD CONSTRAINT check_gender
	CHECK (gender IN ('M', 'F'));