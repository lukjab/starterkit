ALTER TABLE copies
    ADD CONSTRAINT check_copy_status
    CHECK (status IN ('A', 'U', 'B', 'M'));