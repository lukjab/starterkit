package selenium.homepage;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import selenium.SeleniumTest;
import selenium.data.HomePage;

public class HomePageSeleniumTest extends SeleniumTest {

    @Test
    public void shouldUsersLinkExist() {
        //when
        openDevelopmentSiteOnLocalhost();
        //then
        assertTrue(driver.findElement(HomePage.LINK_USERS).isDisplayed());
    }

    @Test
    public void shouldRetrieveValidTitle() {
        //when
        openDevelopmentSiteOnLocalhost();
        //then
        assertEquals(HomePage.TITLE, driver.getTitle());
    }
}
