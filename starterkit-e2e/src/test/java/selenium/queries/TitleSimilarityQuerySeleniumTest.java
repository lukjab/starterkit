package selenium.queries;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.util.List;
import java.util.stream.Collectors;
import org.junit.Test;
import org.openqa.selenium.WebElement;

import selenium.SeleniumTest;
import selenium.data.FindByTitleTab;
import selenium.data.HomePage;

public class TitleSimilarityQuerySeleniumTest extends SeleniumTest {

    private static final String KOSZMAR_TITLE = "Koszmar z ulicy Wiązów";
    private static final String KOSZMAR2_TITLE = "Koszmar z ulicy Wiązów 2: Zemsta Freddy'ego";
    private static final String KOSZMAR3_TITLE = "Koszmar z ulicy Wiązów 3: Wojownicy snów";
    private static final String SIMILAR_MOVIE_TITLE = "Koszmar";

    @Test
    public void shouldFindMoviesWithSimilarTitle() {
        //given
        openDevelopmentSiteOnLocalhost();
        //when
        driver.findElement(HomePage.LINK_FIND_MOVIE).click();
        WebElement findByTitleInput = getElementWhenVisible(FindByTitleTab.FIND_BY_TITLE_INPUT, DEFAULT_TIMEOUT_IN_SECONDS);

        findByTitleInput.sendKeys(SIMILAR_MOVIE_TITLE);
        driver.findElement(FindByTitleTab.SEARCH_BUTTON).click();

        List<WebElement> foundMovies = getElementsWhenVisible(FindByTitleTab.FOUND_MOVIES, DEFAULT_TIMEOUT_IN_SECONDS);

        List<String> foundMoviesTitles = getCorrectTitlesFromWebElements(foundMovies);
        //then
        assertEquals(3, foundMovies.size());
        assertThat(foundMoviesTitles, hasItems(KOSZMAR_TITLE, KOSZMAR2_TITLE, KOSZMAR3_TITLE));
    }

    private List<String> getCorrectTitlesFromWebElements(List<WebElement> webElements) {
        return webElements.parallelStream()
                .map(element -> prepareCorrectTitle(element))
                .collect(Collectors.toList());
    }

    private String prepareCorrectTitle(WebElement elementWithNewlineInTitle) {
        if (elementWithNewlineInTitle.getText().endsWith("\n0")) {
            return elementWithNewlineInTitle.getText().substring(0, elementWithNewlineInTitle.getText().length() - 2);
        } else {
            return elementWithNewlineInTitle.getText();
        }
    }
}
