package selenium.data;

import org.openqa.selenium.By;

public class HomePage {
    public static final By LINK_USERS = By.linkText("Użytkownicy");
    public static final By LINK_FIND_MOVIE = By.linkText("Szukaj filmu");
    public static final String TITLE = "StarterkitWeb";
}
