package selenium.data;

import org.openqa.selenium.By;

public class FindByTitleTab {
    public static final By FIND_BY_TITLE_INPUT = By.name("title");
    public static final By FOUND_MOVIES = By.tagName("li");
    public static final By SEARCH_BUTTON = By.className("search-button");
}
