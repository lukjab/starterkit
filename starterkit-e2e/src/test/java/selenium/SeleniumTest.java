package selenium;

import static org.junit.Assume.assumeFalse;
import static selenium.ChromeWebDriver.LOCALHOST_TITLE;

import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SeleniumTest {
    protected WebDriver driver;
    protected static final Long DEFAULT_TIMEOUT_IN_SECONDS = 10L;

    @Before
    public void prepareDriver() {
        System.setProperty(ChromeWebDriver.DRIVER_NAME, ChromeWebDriver.DRIVER_LOCATION);
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @After
    public void closeAndQuitDriver() {
        driver.close();
        driver.quit();
    }

    protected void openDevelopmentSiteOnLocalhost() {
        driver.get(ChromeWebDriver.ANGULAR_DEVELOPMENT_SERVER_URL);
        assumeFalse(LOCALHOST_TITLE.equals(driver.getTitle()));
    }

    protected WebElement getElementWhenVisible(By locatedBy, Long timeOutInSeconds) {
        return (new WebDriverWait(driver, timeOutInSeconds))
                .until(ExpectedConditions.visibilityOfElementLocated(locatedBy));
    }

    protected List<WebElement> getElementsWhenVisible(By locatedBy, Long timeOutInSeconds) {
        return (new WebDriverWait(driver, timeOutInSeconds))
                .until(ExpectedConditions.visibilityOfAllElementsLocatedBy(locatedBy));
    }
}
