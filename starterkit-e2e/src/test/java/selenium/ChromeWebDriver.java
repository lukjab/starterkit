package selenium;

public class ChromeWebDriver {
    public static final String ANGULAR_DEVELOPMENT_SERVER_URL = "http://localhost:4200";
    public static final String DRIVER_NAME = "webdriver.chrome.driver";
    //poniższa ścieżka zgadza się na moim komputerze, każdy musi sobie web drivera samemu skonfigurować
    public static final String DRIVER_LOCATION = "E:/StarterKit/project/starterkit/chromedriver.exe";
    public static final String LOCALHOST_TITLE = "localhost";
}
