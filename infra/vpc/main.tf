resource "aws_vpc" "starterkit" {
  cidr_block       = "10.0.0.0/16"

  tags = {
    Name = "starterkit"
  }
}

resource "aws_subnet" "starterkit_dev" {
  cidr_block = "10.0.1.0/24"
  vpc_id = aws_vpc.starterkit.id

  tags = {
    Name = "startekit_dev"
  }
}
