resource "aws_ecr_repository" "starterkit" {
  name                 = "starterkit"
  image_tag_mutability = "MUTABLE"
}
