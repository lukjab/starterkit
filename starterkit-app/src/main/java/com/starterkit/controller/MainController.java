package com.starterkit.controller;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class MainController {	
	
	private static final Logger logger = Logger.getLogger(MainController.class);
	
    @RequestMapping(value="/",method = RequestMethod.GET)
    public String homepage(){
    	if(logger.isDebugEnabled()){
			logger.debug("homepage function from MainController was executed.");
		}
    	
        return "index.html";
    }
    
}
