package com.starterkit.controller.services.rentals;

import java.util.List;
import java.util.stream.Collectors;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.starterkit.controller.services.exceptions.NoCopiesAvailableException;
import com.starterkit.controller.services.exceptions.UserNotFoundException;
import com.starterkit.controller.services.rentals.request.NewRentalRequestDTO;
import com.starterkit.controller.services.rentals.request.ReturnRentalsRequestDTO;
import com.starterkit.controller.services.rentals.validator.RentalValidator;

import core.entities.copies.CopyBE;
import core.entities.rentals.RentalBE;
import core.entities.rentals.RentalConverter;
import core.entities.rentals.RentalDTO;
import core.utilities.copies.dao.CopyDAO;
import core.utilities.rentals.RentalDAO;
import core.utilities.users.UserDAO;

@RestController
@RequestMapping("/api/rentals")
public class RentalsService {

    private static final Logger LOGGER = Logger.getLogger(RentalsService.class);

    @Autowired
    private RentalValidator rentalValidator;
    @Autowired
    private CopyDAO copyDAO;
    @Autowired
    private UserDAO userDAO;
    @Autowired
    private RentalDAO rentalDAO;
    @Autowired
    private RentalConverter rentalConverter;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public void rentMovie(final @RequestBody NewRentalRequestDTO newRentalRequest) throws NoCopiesAvailableException, UserNotFoundException {
        List<CopyBE> availableCopiesForMovie = copyDAO.findAllCopiesByMovie(newRentalRequest.getMovieId());
        rentalValidator.validateAvailableCopies(availableCopiesForMovie);

        RentalBE newRental = new RentalBE();
        newRental.setCopy(pickAvailableCopy(availableCopiesForMovie));
        newRental.setUser(userDAO.findById(newRentalRequest.getUserId()));
        LOGGER.debug("Utworzono nowe wypozyczenie dla userId " + newRentalRequest.getUserId()
                + " i movieId " + newRentalRequest.getMovieId());
        //TODO dodać zmianę statusU wypożczonej kopii
        rentalDAO.saveNewRental(newRental);
    }

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public List<RentalDTO> findRentalsByUser(final Long userId) {
        List<RentalBE> rentalsForUser = rentalDAO.findRentalsByUser(userId);
        return rentalsForUser.parallelStream()
                .map(rentalBE -> rentalConverter.convertToDTO(rentalBE))
                .collect(Collectors.toList());
    }

    @ResponseBody
    @RequestMapping(value = "/return", method = RequestMethod.POST)
    public void returnRentalsForUser(final @RequestBody ReturnRentalsRequestDTO returnRentalsRequest) {
        for (Long rentalToReturn : returnRentalsRequest.getRentalsToReturn()) {
            rentalDAO.deleteRental(rentalToReturn);
        }
    }

    private CopyBE pickAvailableCopy(List<CopyBE> availableCopies) {
        CopyBE copy = availableCopies.remove(0);
        while (!rentalValidator.isCopyAvailableForRental(copy)) {
            copy = availableCopies.remove(0);
        }
        return copy;
    }
}