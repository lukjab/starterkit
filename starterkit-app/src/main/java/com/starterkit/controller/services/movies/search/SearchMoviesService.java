package com.starterkit.controller.services.movies.search;

import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.starterkit.controller.services.exceptions.MovieNotFoundException;

import core.entities.movies.MovieBE;
import core.entities.movies.MovieConverter;
import core.entities.movies.MovieDTO;
import core.utilities.movies.MovieDAO;

@RestController
@RequestMapping("/api/searchmovies")
public class SearchMoviesService {
    private final static Double TITLE_SIMILARITY = 0.1;

    @Autowired
    private MovieConverter movieConverter;
    @Autowired
    private MovieDAO movieDAO;

    @RequestMapping(value = "/title", method = RequestMethod.GET)
    public List<MovieDTO> findByTitle(final String title) {
        List<MovieBE> foundMovies = movieDAO.findBySimilarTitle(title, TITLE_SIMILARITY);
        if (foundMovies.isEmpty()) {
            throw new MovieNotFoundException("Nie znaleziono filmu", Response.Status.INTERNAL_SERVER_ERROR);
        }
        return foundMovies.parallelStream()
                .map(movieBE -> movieConverter.convertToDTO(movieBE))
                .collect(Collectors.toList());
    }

    @ResponseBody
    @RequestMapping(value = "/genres", method = RequestMethod.POST)
    public List<MovieDTO> findByGenre(final @RequestBody List<Long> genreIds) {
        return movieDAO.findByGenres(genreIds)
                .parallelStream()
                .map(movieBE -> movieConverter.convertToDTO(movieBE))
                .distinct()
                .collect(Collectors.toList());
    }

    @RequestMapping(value = "/year", method = RequestMethod.GET)
    public List<MovieDTO> findByYear(final Integer year) {
        return movieDAO.findByYear(year)
                .parallelStream()
                .map(movieBE -> movieConverter.convertToDTO(movieBE))
                .collect(Collectors.toList());
    }
}
