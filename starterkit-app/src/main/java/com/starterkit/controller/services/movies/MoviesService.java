package com.starterkit.controller.services.movies;

import static java.util.stream.Collectors.toList;

import java.util.List;

import core.entities.movies.MovieBE;
import core.entities.movies.MovieConverter;
import core.entities.movies.MovieDTO;
import core.utilities.movies.MovieDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/movies")
public class MoviesService {
    @Autowired
    private MovieDAO movieDAO;
    @Autowired
    private MovieConverter movieConverter;

    @RequestMapping(method = RequestMethod.GET)
    public List<MovieDTO> findAllMovies() {
        List<MovieBE> result = movieDAO.findAllMovies();

        return result.parallelStream()
                .map(movieConverter::convertToDTO)
                .collect(toList());
    }

    @RequestMapping(value = "/premierDateYears", method = RequestMethod.GET)
    public List<Integer> findAllPremierDateYears() {
        return movieDAO.findAllPremierDateYears();
    }

    @RequestMapping(method = RequestMethod.POST)
    public void saveMovie(final MovieDTO movieDTO) {
        MovieBE movieToSave = movieConverter.convertToBE(movieDTO);
        if (movieToSave.getId() == null) {
            movieDAO.saveNewMovie(movieToSave);
        } else {
            movieDAO.updateMovie(movieToSave);
        }
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public void deleteMovie(final Long id) {
        movieDAO.deleteMovie(id);
    }

    @RequestMapping(value = "/movieInfo", method = RequestMethod.GET)
    public MovieDTO findMovieById(final Long movieId) {
        return movieConverter.convertToDTO(movieDAO.findById(movieId));
    }
}