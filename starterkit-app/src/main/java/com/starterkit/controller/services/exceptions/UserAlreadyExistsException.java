package com.starterkit.controller.services.exceptions;

import javax.ws.rs.ServerErrorException;
import javax.ws.rs.core.Response;

public class UserAlreadyExistsException extends ServerErrorException {
	public UserAlreadyExistsException(String message, Response.Status status) {
		super(message, status);
	}
}
