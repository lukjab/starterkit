package com.starterkit.controller.services.rentals.validator;

import java.util.List;
import javax.ws.rs.core.Response;

import com.starterkit.controller.services.exceptions.NoCopiesAvailableException;
import core.entities.copies.CopyBE;
import core.utilities.rentals.RentalDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RentalValidator {
    @Autowired
    private RentalDAO rentalDAO;

    public void validateAvailableCopies(List<CopyBE> availableCopies) throws NoCopiesAvailableException {
        if (availableCopies.size() == 0) {
            throw new NoCopiesAvailableException("Nie ma żadnych egzemplarzy", Response.Status.INTERNAL_SERVER_ERROR);
        }
        Integer numberOfAvailableCopies = 0;
        for (CopyBE copy : availableCopies) {
            numberOfAvailableCopies += isCopyAvailableForRental(copy) ? 1 : 0;
        }
        if (numberOfAvailableCopies == 0) {
            throw new NoCopiesAvailableException("Wszystkie kopie są aktualnie wypożyczone", Response.Status.INTERNAL_SERVER_ERROR);
        }
    }

    public boolean isCopyAvailableForRental(CopyBE copy) {
        return rentalDAO.findRentalByCopy(copy.getId()) == null;
    }
}
