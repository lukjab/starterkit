package com.starterkit.controller.services.rentals.request;

import java.util.List;

public class ReturnRentalsRequestDTO {
    private List<Long> rentalsToReturn;

    public List<Long> getRentalsToReturn() {
        return rentalsToReturn;
    }

    public void setRentalsToReturn(List<Long> rentalsToReturn) {
        this.rentalsToReturn = rentalsToReturn;
    }
}
