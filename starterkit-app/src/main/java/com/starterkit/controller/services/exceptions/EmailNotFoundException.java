package com.starterkit.controller.services.exceptions;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.core.Response;

public class EmailNotFoundException extends ClientErrorException {
    public EmailNotFoundException(String message) {
        super(message, Response.Status.NOT_FOUND);
    }
}
