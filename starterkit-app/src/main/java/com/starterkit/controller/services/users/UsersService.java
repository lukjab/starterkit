package com.starterkit.controller.services.users;

import static java.util.stream.Collectors.toList;

import javax.ws.rs.QueryParam;
import java.util.List;
import javax.ws.rs.core.Response;


import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.base.Strings;
import com.starterkit.controller.services.exceptions.EmailNotFoundException;
import com.starterkit.controller.services.exceptions.UserAlreadyExistsException;
import com.starterkit.controller.services.exceptions.UserNotFoundException;

import core.entities.users.UserBE;
import core.entities.users.UserConverter;
import core.entities.users.UserDTO;
import core.utilities.users.UserDAO;

@RestController
@RequestMapping("/api/users")
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class UsersService {

    private UserDAO userDAO;
    private UserConverter userConverter;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public void saveUser(@RequestBody UserDTO userDTO) {
        UserBE userToSave = userConverter.convertToBE(userDTO);
        
        if(Strings.isNullOrEmpty(userDTO.getEmail())) {
        	throw new EmailNotFoundException("Nie podano adresu email");
        }
        
        if(userDAO.doesUserExist(userToSave.getEmail())) {
			throw new UserAlreadyExistsException("Użytkownik o podanym adresie email już istnieje", Response.Status.INTERNAL_SERVER_ERROR);        	
        } else {
        	userDAO.saveNewUser(userToSave);
        }
    }

    
    @RequestMapping(method = RequestMethod.GET)
    public List<UserDTO> findAllUsers() {
        List<UserBE> result = userDAO.findAllUsers();

        return result.parallelStream()
                .map(userConverter::convertToDTO)
                .collect(toList());
    }

    @RequestMapping(value = "/userInfo", method = RequestMethod.GET)
    public UserDTO findUserById(@QueryParam("id") Long id) throws UserNotFoundException {
        return userConverter.convertToDTO(userDAO.findById(id));
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public void deleteUser(Long id) {
        userDAO.deleteUser(id);
    }
}
