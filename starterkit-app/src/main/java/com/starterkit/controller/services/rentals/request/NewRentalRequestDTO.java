package com.starterkit.controller.services.rentals.request;


public class NewRentalRequestDTO {
    private Long movieId;
    private Long userId;

    public Long getMovieId() {
        return movieId;
    }

    public void setMovieId(Long movieId) {
        this.movieId = movieId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
