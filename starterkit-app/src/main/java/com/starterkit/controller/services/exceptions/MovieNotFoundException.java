package com.starterkit.controller.services.exceptions;

import javax.ws.rs.ServerErrorException;
import javax.ws.rs.core.Response;

public class MovieNotFoundException extends ServerErrorException {
    public MovieNotFoundException(String message, Response.Status status) {
        super(message, status);
    }
}
