package com.starterkit.controller.services.exceptions;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.core.Response;

public class UserNotFoundException extends ClientErrorException {
	public UserNotFoundException(String message) {
		super(message, Response.Status.NOT_FOUND);
	}
}