package com.starterkit.controller.services.exceptions;

import javax.ws.rs.ServerErrorException;
import javax.ws.rs.core.Response;

/**
 * @author Lukasz Jablonski, Capgemini Technology Services
 */
public class NoCopiesAvailableException extends ServerErrorException {

    public NoCopiesAvailableException(String message, Response.Status status) {
        super(message, status);
    }
}
