package com.starterkit.controller.services.genres;

import static java.util.stream.Collectors.toList;

import java.util.List;

import core.entities.genres.GenreBE;
import core.entities.genres.GenreConverter;
import core.entities.genres.GenreDTO;
import core.utilities.genres.GenreDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/genres")
public class GenresService {

    @Autowired
    private GenreConverter genreConverter;

    @Autowired
    private GenreDAO genreDAO;

    @RequestMapping(method = RequestMethod.POST)
    public void saveGenre(final GenreDTO genreDTO) {
        GenreBE genreToSave = genreConverter.convertToBE(genreDTO);
        if (genreToSave.getId() == null) {
            genreDAO.saveGenre(genreToSave);
        } else {
            genreDAO.updateGenre(genreToSave);
        }
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<GenreDTO> findAllGenres() {
        List<GenreBE> result = genreDAO.findAllGenres();
        return result.parallelStream()
                .map(genreConverter::convertToDTO)
                .collect(toList());
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public void deleteGenre(final Long id) {
        genreDAO.deleteGenre(id);
    }

    @RequestMapping(value = "/genreInfo", method = RequestMethod.POST)
    public GenreDTO findGenreById(final Long id) {
        return genreConverter.convertToDTO(genreDAO.findById(id));
    }
}
