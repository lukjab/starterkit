package com.starterkit.configuration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.LocalEntityManagerFactoryBean;

@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackages = { "com.starterkit", "core.utilities", "core.entities" })
// @PropertySource({"classpath:application.properties"}) na razie nie używamy
// tego, Hibernate sam sobie ogarnia propercje przez persistence.xml
public class WebAppInitializer extends SpringBootServletInitializer {
	public static void main(String[] args) throws Exception {
		SpringApplication.run(WebAppInitializer.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(WebAppInitializer.class);
	}

	@Bean
	public LocalEntityManagerFactoryBean entityManagerFactory() {
		LocalEntityManagerFactoryBean factoryBean = new LocalEntityManagerFactoryBean();
		factoryBean.setPersistenceUnitName("JPA");
		return factoryBean;
	}
}
