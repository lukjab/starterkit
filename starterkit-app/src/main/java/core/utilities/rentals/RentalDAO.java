package core.utilities.rentals;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import core.entities.rentals.RentalBE;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional
public class RentalDAO {

    private static final Logger logger = Logger.getLogger(RentalDAO.class);

    @PersistenceContext
    private EntityManager entityManager;

    public void saveNewRental(RentalBE rentalBE) {
        try {
            entityManager.persist(rentalBE);
        } catch (Exception e) {
            logger.debug(e.getMessage());
        }
    }

    public void updateRental(RentalBE rentalBE) {
        try {
            entityManager.merge(rentalBE);
        } catch (Exception e) {
            logger.debug(e.getMessage());
        }
    }

    public void deleteRental(Long id) {
        RentalBE rentalFromId = entityManager.find(RentalBE.class, id);
        try {
            entityManager.remove(rentalFromId);
        } catch (Exception e) {
            logger.debug(e.getMessage()); //TODO: mozna dać osobny wyjatek w przypadku braku id
        }
    }

    public RentalBE findRentalById(Long id) {
        return entityManager.find(RentalBE.class, id);
    }

    public List<RentalBE> findRentalsByUser(Long userId) {
        TypedQuery<RentalBE> findByUser = entityManager.createNamedQuery(RentalBE.findRentalsByUserQueryName, RentalBE.class);
        findByUser.setParameter(RentalBE.PARAM_USER_ID, userId);
        return findByUser.getResultList();
    }

    public List<RentalBE> findAllRentals() {
        TypedQuery<RentalBE> findAllRentals = entityManager.createNamedQuery(RentalBE.findAllRentalsQueryName, RentalBE.class);
        return findAllRentals.getResultList();
    }

    public RentalBE findRentalByCopy(final Long copyId) {
        TypedQuery<RentalBE> findRentalByCopy = entityManager.createNamedQuery(RentalBE.findRentalByCopyQueryName, RentalBE.class);
        findRentalByCopy.setParameter(RentalBE.PARAM_COPY_ID, copyId);
        List<RentalBE> result = findRentalByCopy.getResultList();
        return result.isEmpty() ? null : result.get(0);
    }

}
