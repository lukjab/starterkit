package core.utilities.users;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.starterkit.controller.services.exceptions.UserNotFoundException;

import core.entities.users.UserBE;


@Component
@Transactional
public class UserDAO {
	
	private static final Logger logger = Logger.getLogger(UserDAO.class);

	@PersistenceContext
	private EntityManager entityManager;

	public UserBE findById(Long id) throws UserNotFoundException {
		return Optional.ofNullable(entityManager.find(UserBE.class, id))
				.orElseThrow(() -> new UserNotFoundException("Nie znaleziono użytkownika o podanym id"));
	}
	
	public UserBE findByEmail(String email) throws UserNotFoundException {
		TypedQuery<UserBE> findByEmail = entityManager.createNamedQuery(UserBE.findUserByEmailQueryName, UserBE.class);
		findByEmail.setParameter(UserBE.PARAM_EMAIL, email);
		return findByEmail.getResultList().stream()
				.findFirst()
				.orElseThrow(() -> new UserNotFoundException("Nie znaleziono użytkownika o podanym adresie email"));
	}
	
	public void saveNewUser(UserBE userBE) {
		try {
			entityManager.persist(userBE);
		} catch (Exception e) {
			logger.debug(e.getMessage());
		}
	}

	public void updateUser(UserBE userBE) {
		try {
			entityManager.merge(userBE);
		} catch (Exception e) {
			logger.debug(e.getMessage());
		}
	}

	public void deleteUser(Long id) {
		UserBE userFromId = entityManager.find(UserBE.class, id);
		try {
			entityManager.remove(userFromId);
		} catch (Exception e) {
			logger.debug(e.getMessage());
		}
	}

	public List<UserBE> findAllUsers() {
		TypedQuery<UserBE> findAllUsersQuery = entityManager.createQuery(UserBE.findAllUsersSQL, UserBE.class);
		return findAllUsersQuery.getResultList();
	}
	
	public boolean doesUserExist(String email) {
		try {
			findByEmail(email);
			return true;
		} catch (UserNotFoundException e) {
			return false;
		}
	}
}
