package core.utilities.movies;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import core.entities.movies.MovieBE;

@Component
@Transactional
public class MovieDAO {
	
	private static final Logger LOGGER = Logger.getLogger(MovieDAO.class);

	@PersistenceContext
	private EntityManager entityManager;

	public MovieBE findById(Long id) {		
		return entityManager.find(MovieBE.class, id);
	}

	public MovieBE findByTitle(String title){
		TypedQuery<MovieBE> findByTitle = entityManager.createNamedQuery(MovieBE.findMoviesByTitleQueryName, MovieBE.class);
		findByTitle.setParameter(MovieBE.PARAM_TITLE, title);
		List<MovieBE> result = findByTitle.getResultList();
		return result.isEmpty() ? null : result.get(0);
	}

    public List<MovieBE> findBySimilarTitle(String title, Double similarity) {
		TypedQuery<MovieBE> findBySimilarTitle = entityManager.createNamedQuery(MovieBE.findMoviesBySimilarTitleQueryName, MovieBE.class);
		findBySimilarTitle.setParameter(MovieBE.PARAM_TITLE, title);
		findBySimilarTitle.setParameter(MovieBE.PARAM_SIMILARITY, similarity);
		return findBySimilarTitle.getResultList();
	}

	public List<MovieBE> findByGenres(List<Long> genereIds) {
		Query findByGenres = entityManager.createNamedQuery(MovieBE.findMoviesByGenresQueryName);
		findByGenres.setParameter(MovieBE.PARAM_GENRES, genereIds);
		return findByGenres.getResultList();
	}

	public List<MovieBE> findByYear(Integer year) {
		TypedQuery<MovieBE> findByYear = entityManager.createNamedQuery(MovieBE.findMoviesByYearQueryName, MovieBE.class);
		findByYear.setParameter(MovieBE.PARAM_YEAR, year);
		return findByYear.getResultList();
	}

	public void saveNewMovie(MovieBE movieBE) {
		try {
			entityManager.persist(movieBE);
		} catch (Exception e) {
			LOGGER.debug(e.getMessage());
		}
	}

	public void updateMovie(MovieBE movieBE) {
		try {
			entityManager.merge(movieBE);
		} catch (Exception e) {
			LOGGER.debug(e.getMessage());
		}
	}

	public void deleteMovie(Long id) {
		MovieBE movieFromId = entityManager.find(MovieBE.class, id);
		try {
			entityManager.remove(movieFromId);
		} catch (Exception e) {
			LOGGER.debug(e.getMessage()); //TODO: mozna dać osobny wyjatek w przypadku braku id
		}
	}

	public List<MovieBE> findAllMovies() {
		TypedQuery<MovieBE> findAllMovies = entityManager.createNamedQuery(MovieBE.findAllMoviesQueryName, MovieBE.class);
		
		return findAllMovies.getResultList();
	}

	public List<Integer> findAllPremierDateYears() {
		TypedQuery<Integer> findAllPremierYears = entityManager.createNamedQuery(MovieBE.findAllPremierDateYearsQueryName, Integer.class);
		return findAllPremierYears.getResultList();
	}
}