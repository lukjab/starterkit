package core.utilities.genres;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import core.entities.genres.GenreBE;

@Component
@Transactional
public class GenreDAO {
	
	private static final Logger logger = Logger.getLogger(GenreDAO.class);

	@PersistenceContext
	private EntityManager entityManager;

	public GenreBE findById(Long id) {		
		return entityManager.find(GenreBE.class, id);
	}

	public void saveGenre(GenreBE genreBE) {
		try {
			entityManager.persist(genreBE);
		} catch (Exception e) {
			logger.debug(e.getMessage());
		}
	}

	public void updateGenre(GenreBE genreBE) {
		try {
			entityManager.merge(genreBE);
		} catch (Exception e) {
			logger.debug(e.getMessage());
		}
	}

	public void deleteGenre(Long id) {
		GenreBE genreFromId = entityManager.find(GenreBE.class, id);
		try {
			entityManager.remove(genreFromId);
		} catch (Exception e) {
			logger.debug(e.getMessage());
		}
	}

	public List<GenreBE> findAllGenres() {
		TypedQuery<GenreBE> findAllGenresQuery = entityManager.createQuery(GenreBE.findAllGenresSQL, GenreBE.class);
		
		return findAllGenresQuery.getResultList();
	}

}
