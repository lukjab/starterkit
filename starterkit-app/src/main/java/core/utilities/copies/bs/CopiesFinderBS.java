package core.utilities.copies.bs;

import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;

import core.entities.copies.CopyBE;
import core.entities.rentals.RentalBE;
import core.utilities.copies.dao.CopyDAO;
import core.utilities.rentals.RentalDAO;

public class CopiesFinderBS {
    @Autowired
    private CopyDAO copyDAO;
    @Autowired
    private RentalDAO rentalDAO;

    public List<CopyBE> findAllCopies() {
        List<CopyBE> allCopies = copyDAO.findAllCopies().stream()
                .collect(Collectors.toList());

        return allCopies;
    }

    public List<CopyBE> findAllAvailableCopies() {
        List<CopyBE> allAvailableCopies = copyDAO.findAllAvailableCopies().stream()
                .collect(Collectors.toList());

        return allAvailableCopies;
    }

    public List<CopyBE> findAllCopiesByMovie(Long movieId) {
        List<CopyBE> allCopiesForMovie = copyDAO.findAllCopiesByMovie(movieId)
                .stream()
                .collect(Collectors.toList());

        return allCopiesForMovie;
    }

    public List<CopyBE> findAllRentedCopiesForUser(Long userId) {
        List<RentalBE> rentalsForUser = rentalDAO.findRentalsByUser(userId);

        return rentalsForUser.stream()
                .map(rentalBE -> rentalBE.getCopy())
                .collect(Collectors.toList());
    }
}
