package core.utilities.copies.bs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import core.entities.copies.CopyBE;
import core.entities.copies.status.CopyStatus;
import core.utilities.copies.dao.CopyDAO;

@Service
public class CopiesCreatorBS {
    @Autowired
    private CopyDAO copyDAO;

    public void saveNewAvailableCopy(CopyBE copyToSave) {
        copyToSave.setStatus(CopyStatus.AVAILABLE);

        copyDAO.saveNewCopy(copyToSave);
    }
}
