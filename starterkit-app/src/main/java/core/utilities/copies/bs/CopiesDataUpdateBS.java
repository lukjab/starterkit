package core.utilities.copies.bs;

import javax.inject.Inject;
import org.springframework.stereotype.Service;

import core.entities.copies.CopyBE;
import core.entities.copies.status.CopyStatus;
import core.utilities.copies.dao.CopyDAO;

@Service
public class CopiesDataUpdateBS {

    @Inject
    private CopyDAO copyDAO;

    public void setMissingStatusWithDescription(CopyBE copyToUpdate, String description) {
        updateStatusAndDescriptionForCopy(copyToUpdate, CopyStatus.MISSING, description);
    }

    public void setBrokenStatusWithDescription(CopyBE copyToUpdate, String description) {
        updateStatusAndDescriptionForCopy(copyToUpdate, CopyStatus.BROKEN, description);
    }

    private void updateStatusAndDescriptionForCopy(CopyBE copyToUpdate, CopyStatus status, String description) {
        copyToUpdate.setStatus(status);
        copyToUpdate.setDescription(description);
        copyDAO.updateCopy(copyToUpdate);
    }
}
