package core.utilities.copies.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import core.entities.copies.CopyBE;

@Component
@Transactional
public class CopyDAO {

    private static final Logger logger = Logger.getLogger(CopyDAO.class);

    @PersistenceContext
    private EntityManager entityManager;

    public List<CopyBE> findAllCopies() {
        TypedQuery<CopyBE> findAllCopies = entityManager.createNamedQuery(CopyBE.FIND_ALL_COPIES_QUERY_NAME, CopyBE.class);

        return findAllCopies.getResultList();
    }

    public List<CopyBE> findAllAvailableCopies() {
        TypedQuery<CopyBE> findAllAvailableCopies = entityManager.createNamedQuery(CopyBE.FIND_ALL_AVAILABLE_COPIES_QUERY_NAME, CopyBE.class);

        return findAllAvailableCopies.getResultList();
    }

    public List<CopyBE> findAllCopiesByMovie(Long movieId) {
        TypedQuery<CopyBE> findAllCopiesByMovie = entityManager.createNamedQuery(CopyBE.FIND_ALL_COPIES_BY_MOVIE_QUERY_NAME, CopyBE.class);
        findAllCopiesByMovie.setParameter(CopyBE.PARAM_MOVIE_ID, movieId);

        return findAllCopiesByMovie.getResultList();
    }

    public List<CopyBE> findAllAvailableCopiesByMovie(Long movieId) {
        TypedQuery<CopyBE> findAllAvailableCopiesByMovie = entityManager.createNamedQuery(CopyBE.FIND_ALL_AVAILABLE_COPIES_BY_MOVIE_QUERY_NAME, CopyBE.class);
        findAllAvailableCopiesByMovie.setParameter(CopyBE.PARAM_MOVIE_ID, movieId);

        return findAllAvailableCopiesByMovie.getResultList();
    }

    public CopyBE findCopyById(Long id) {
        return entityManager.find(CopyBE.class, id);
    }

    public void saveNewCopy(CopyBE copyBE) {
        try {
            entityManager.persist(copyBE);
        } catch (Exception e) {
            logger.debug(e.getMessage());
        }
    }

    public void updateCopy(CopyBE copyBE) {
        try {
            entityManager.merge(copyBE);
        } catch (Exception e) {
            logger.debug(e.getMessage());
        }
    }

    public void deleteCopy(Long id) {
        CopyBE copyById = entityManager.find(CopyBE.class, id);
        entityManager.remove(copyById);
    }
}
