package core.entities.movies;

import static core.entities.movies.MovieBE.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

import core.entities.copies.CopyBE;
import core.entities.genres.GenreBE;

@Entity
@Table(name = "MOVIES", catalog = "public")
@NamedQueries({
        @NamedQuery(name = findAllMoviesQueryName, query = "SELECT mov FROM MovieBE mov"),
        @NamedQuery(name = findMoviesByTitleQueryName, query = "SELECT mov FROM MovieBE mov WHERE mov.title = :" + PARAM_TITLE),
        @NamedQuery(name = findMoviesBySimilarTitleQueryName, query = "SELECT movie FROM MovieBE movie WHERE similarity(title, :" + PARAM_TITLE + ") >= :" + PARAM_SIMILARITY),
        @NamedQuery(name = findMoviesByGenresQueryName, query = "SELECT mov FROM MovieBE mov JOIN mov.genres genres WHERE genres.id IN :" + PARAM_GENRES),
        @NamedQuery(name = findMoviesByYearQueryName, query = "SELECT mov FROM MovieBE mov WHERE YEAR(mov.premierDate) =:" + PARAM_YEAR),
        @NamedQuery(name = findAllPremierDateYearsQueryName, query = "SELECT DISTINCT YEAR(movie.premierDate) FROM MovieBE movie ORDER BY YEAR(movie.premierDate)")
})
public class MovieBE {
    public static final String findAllMoviesQueryName = "findAllMovies";
    public static final String findMoviesByTitleQueryName = "findMoviesByTitle";
    public static final String findMoviesBySimilarTitleQueryName = "findMoviesBySimilarTitle";
    public static final String findMoviesByGenresQueryName = "findMoviesByGenres";
    public static final String findMoviesByYearQueryName = "findMoviesByName";
    public static final String findAllPremierDateYearsQueryName = "findAllPremierDateYears";

    public static final String PARAM_TITLE = "title";
    public static final String PARAM_GENRES = "genres";
    public static final String PARAM_YEAR = "year";
    public static final String PARAM_SIMILARITY = "similarity";

    @Id
    @GeneratedValue(generator = "movies_id_seq")
    @SequenceGenerator(name = "movies_id_seq", sequenceName = "movies_id_seq", allocationSize = 1)
    @Column(name = "id")
    private Long id;

    @Column(name = "title")
    private String title;

    @Column(name = "director")
    private String director;

    @Column(name = "premier_date")
    private LocalDate premierDate;

    @Column(name = "description")
    private String description;

    @Column(name = "poster_file")
    private String posterFile;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "MAP_MOVIE_GENRE", catalog = "postgres", joinColumns = {
            @JoinColumn(name = "fk_mov", nullable = false, updatable = false)},
            inverseJoinColumns = {@JoinColumn(name = "fk_gen",
                    nullable = false, updatable = false)})
    private Set<GenreBE> genres;

    @OneToMany(mappedBy = "movie")
    private Set<CopyBE> copies;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public LocalDate getPremierDate() {
        return premierDate;
    }

    public void setPremierDate(LocalDate premierDate) {
        this.premierDate = premierDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<GenreBE> getGenres() {
        return genres;
    }

    public void setGenres(Set<GenreBE> genres) {
        this.genres = genres;
    }

    public String getPosterFile() {
        return posterFile;
    }

    public Set<CopyBE> getCopies() {
        return copies;
    }
}
