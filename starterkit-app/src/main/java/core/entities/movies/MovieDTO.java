package core.entities.movies;

import java.time.LocalDate;
import java.util.Objects;
import org.springframework.format.annotation.DateTimeFormat;

public class MovieDTO {
    private Long id;
    private String title;
    private String director;
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private LocalDate premierDate;
    private String description;
    private Integer numberOfAvailableCopies;
    private String posterFile;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public LocalDate getPremierDate() {
        return premierDate;
    }

    public void setPremierDate(LocalDate premierDate) {
        this.premierDate = premierDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getNumberOfAvailableCopies() {
        return numberOfAvailableCopies;
    }

    public void setNumberOfAvailableCopies(Integer numberOfAvailableCopies) {
        this.numberOfAvailableCopies = numberOfAvailableCopies;
    }

    public String getPosterFile() {
        return posterFile;
    }

    public void setPosterFile(String posterFile) {
        this.posterFile = posterFile;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MovieDTO)) {
            return false;
        }
        MovieDTO movieToCompare = (MovieDTO) o;

        return this.id.equals(movieToCompare.id)
                && this.title.equals(movieToCompare.title)
                && this.director.equals(movieToCompare.director)
                && this.premierDate.equals(movieToCompare.premierDate)
                && this.description.equals(movieToCompare.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, director, premierDate, description);
    }

}
