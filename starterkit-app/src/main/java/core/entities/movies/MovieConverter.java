package core.entities.movies;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import core.utilities.copies.dao.CopyDAO;

@Component
public class MovieConverter {

    @Autowired
    private CopyDAO copyDAO;

    public MovieBE convertToBE(MovieDTO movieDTO) {
        MovieBE movieBE = new MovieBE();

        movieBE.setId(movieDTO.getId());
        movieBE.setTitle(movieDTO.getTitle());
        movieBE.setDirector(movieDTO.getDirector());
        movieBE.setPremierDate(movieDTO.getPremierDate());
        movieBE.setDescription(movieDTO.getDescription());

        return movieBE;
    }

    public MovieDTO convertToDTO(MovieBE movieBE) {
        MovieDTO movieDTO = new MovieDTO();

        movieDTO.setId(movieBE.getId());
        movieDTO.setTitle(movieBE.getTitle());
        movieDTO.setDirector(movieBE.getDirector());
        movieDTO.setPremierDate(movieBE.getPremierDate());
        movieDTO.setDescription(movieBE.getDescription());
        movieDTO.setNumberOfAvailableCopies(copyDAO.findAllAvailableCopiesByMovie(movieBE.getId()).size());
        movieDTO.setPosterFile(movieBE.getPosterFile());

        return movieDTO;
    }
}
