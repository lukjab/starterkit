package core.entities.copies;

import static core.entities.copies.CopyBE.*;

import javax.persistence.*;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import core.entities.copies.status.CopyStatus;
import core.entities.copies.status.CopyStatusType;
import core.entities.movies.MovieBE;

@TypeDefs({@TypeDef(name = "CopyStatusType", typeClass = CopyStatusType.class)})
@Entity
@Table(name = "COPIES", catalog = "public")
@NamedQueries({
        @NamedQuery(name = FIND_ALL_COPIES_QUERY_NAME, query = "SELECT copy FROM CopyBE copy"),
        @NamedQuery(name = FIND_ALL_COPIES_BY_MOVIE_QUERY_NAME, query = "SELECT copy FROM CopyBE copy join copy.movie movie WHERE movie.id = :" + PARAM_MOVIE_ID),
        @NamedQuery(name = FIND_ALL_AVAILABLE_COPIES_BY_MOVIE_QUERY_NAME, query = "SELECT copy FROM CopyBE copy join copy.movie movie WHERE movie.id = :" + PARAM_MOVIE_ID
                + " AND copy.id NOT IN (SELECT rental.copy.id FROM RentalBE rental)"),
        @NamedQuery(name = FIND_ALL_AVAILABLE_COPIES_QUERY_NAME, query = "SELECT copy FROM CopyBE copy WHERE copy.status = 'A'")
})
public class CopyBE {
    public static final String FIND_ALL_COPIES_QUERY_NAME = "findAllCopies";
    public static final String FIND_ALL_COPIES_BY_MOVIE_QUERY_NAME = "findAllCopiesByMovie";
    public static final String FIND_ALL_AVAILABLE_COPIES_BY_MOVIE_QUERY_NAME = "findAllAvailableCopiesByMovie";
    public static final String FIND_ALL_AVAILABLE_COPIES_QUERY_NAME = "findAllAvailableCopies";

    public static final String PARAM_MOVIE_ID = "movieId";

    @Id
    @GeneratedValue(generator = "copy_id_seq")
    @SequenceGenerator(name = "copy_id_seq", sequenceName = "copy_id_seq", allocationSize = 1)
    @Column(name = "id")
    private Long id;

    @Column(name = "status")
    @Type(type = "CopyStatusType")
    private CopyStatus status;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "fk_mov")
    private MovieBE movie;

    @Column(name = "description")
    private String description;

    @Transient
    public String getMovieTitle() {
        return this.movie.getTitle();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CopyStatus getStatus() {
        return status;
    }

    public void setStatus(CopyStatus status) {
        this.status = status;
    }

    public MovieBE getMovie() {
        return movie;
    }

    public void setMovie(MovieBE movie) {
        this.movie = movie;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
