package core.entities.copies;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import core.entities.copies.status.CopyStatus;
import core.entities.movies.MovieBE;
import core.utilities.movies.MovieDAO;

@Component
public class CopyConverter {
    @Autowired
    private MovieDAO movieDAO;

    public CopyBE convertToBE(CopyDTO copyDTO) {
        MovieBE movie = movieDAO.findById(copyDTO.getMovieId());

        CopyBE copyBE = new CopyBE();
        copyBE.setId(copyDTO.getId());
        copyBE.setMovie(movie);
        copyBE.setStatus(CopyStatus.fromValue(copyDTO.getStatus()));
        copyBE.setDescription(copyDTO.getDescription());

        return copyBE;
    }

    public CopyDTO convertToDTO(CopyBE copyBE) {
        CopyDTO copyDTO = new CopyDTO();

        copyDTO.setId(copyBE.getId());
        copyDTO.setMovieId(copyBE.getMovie().getId());
        copyDTO.setMovieTitle(copyBE.getMovie().getTitle());
        copyDTO.setStatus(copyBE.getStatus().getValue());
        copyDTO.setDescription(copyBE.getDescription());

        return copyDTO;
    }
}
