package core.entities.copies.status;

public enum CopyStatus {

    AVAILABLE('A'),
    UNAVAILABLE('U'), //TODO zamienić Unavailable na rented - trzeba będzie również zmienić typ na bazie danych
    BROKEN('B'),
    MISSING('M');

    private Character value;

    public Character getValue() {
        return value;
    }

    CopyStatus(Character value) {
        this.value = value;
    }

    public static CopyStatus fromValue(Character statusAbbreviation) {
        for (CopyStatus copyStatus : CopyStatus.values())
            if (copyStatus.getValue().equals(statusAbbreviation)) {
                return copyStatus;
            }
        throw new UnsupportedOperationException(
                "Skrót statusu kopii  " + statusAbbreviation + " nie jest poprawny!");
    }

}
