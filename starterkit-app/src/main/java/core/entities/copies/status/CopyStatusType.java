package core.entities.copies.status;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.usertype.UserType;

public class CopyStatusType implements UserType {
    private int[] types = {Types.CHAR};

    @Override
    public int[] sqlTypes() {
        return types;
    }

    @Override
    public Class returnedClass() {
        return CopyStatus.class;
    }

    @Override
    public boolean equals(Object o, Object o1) throws HibernateException {
        return (o == o1) ||
                ((o != null) && (o1 != null) && (o.equals(o1)));
    }

    @Override
    public int hashCode(Object o) throws HibernateException {
        return o.hashCode();
    }

    @Override
    public Object nullSafeGet(ResultSet resultSet, String[] names, SharedSessionContractImplementor session, Object owner) throws HibernateException, SQLException {
        Character characterFromDB = resultSet.getString(names[0]).charAt(0);
        if(resultSet.wasNull()){
            return null;
        }
        return CopyStatus.fromValue(characterFromDB);
    }

    @Override
    public void nullSafeSet(PreparedStatement preparedStatement, Object value, int index, SharedSessionContractImplementor session) throws HibernateException, SQLException {
        if(value==null){
            preparedStatement.setNull(index, Types.CHAR);
        } else {
            Character genderCharacter = ((CopyStatus) value).getValue();
            preparedStatement.setString(index, genderCharacter.toString());
        }
    }

    @Override
    public Object deepCopy(Object o) throws HibernateException {
        return o;
    }

    @Override
    public boolean isMutable() {
        return false;
    }

    @Override
    public Serializable disassemble(Object value) throws HibernateException {
        return (Serializable) value;
    }

    @Override
    public Object assemble(Serializable serializable, Object o) throws HibernateException {
        return serializable;
    }

    @Override
    public Object replace(Object original, Object target, Object owner) throws HibernateException {
        return original;
    }
}
