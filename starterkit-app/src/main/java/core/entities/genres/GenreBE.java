package core.entities.genres;

import javax.persistence.*;
import java.util.Set;

import core.entities.movies.MovieBE;

@Entity
@Table(name = "GENRES", catalog = "public")
public class GenreBE {
    public static final String findAllGenresSQL = "FROM GenreBE";
    @Id
    @GeneratedValue(generator = "genre_id_seq")
    @SequenceGenerator(name = "genre_id_seq", sequenceName = "genre_id_seq", allocationSize = 1)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "MAP_MOVIE_GENRE", catalog = "postgres", joinColumns = {
            @JoinColumn(name = "fk_gen", nullable = false, updatable = false)},
            inverseJoinColumns = {@JoinColumn(name = "fk_mov",
                    nullable = false, updatable = false)})
    private Set<MovieBE> movies;

    public GenreBE() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<MovieBE> getMovies() {
        return movies;
    }

    public void setMovies(Set<MovieBE> movies) {
        this.movies = movies;
    }

}
