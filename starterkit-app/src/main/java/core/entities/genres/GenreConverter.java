package core.entities.genres;

import org.springframework.stereotype.Component;

@Component
public class GenreConverter {

    public GenreBE convertToBE(GenreDTO genreDTO) {
        GenreBE genreBE = new GenreBE();

        genreBE.setId(genreDTO.getId());
        genreBE.setName(genreDTO.getName());
        genreBE.setDescription(genreDTO.getDescription());

        return genreBE;
    }

    public GenreDTO convertToDTO(GenreBE genreBE) {
        GenreDTO genreDTO = new GenreDTO();

        genreDTO.setId(genreBE.getId());
        genreDTO.setName(genreBE.getName());
        genreDTO.setDescription(genreBE.getDescription());

        return genreDTO;
    }
}


