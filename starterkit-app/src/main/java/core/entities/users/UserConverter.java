package core.entities.users;

import core.entities.users.gender.Gender;
import org.springframework.stereotype.Component;

@Component
public class UserConverter {

    public UserBE convertToBE(UserDTO userDTO) {
        UserBE userBE = new UserBE();

        userBE.setId(userDTO.getId());
        userBE.setPassword(userDTO.getPassword());
        userBE.setEmail(userDTO.getEmail());
        userBE.setGender(Gender.fromValue(userDTO.getGender()));
        userBE.setDateOfBirth(userDTO.getDateOfBirth());

        return userBE;
    }

    public UserDTO convertToDTO(UserBE userBE) {
        UserDTO userDTO = new UserDTO();

        userDTO.setId(userBE.getId());
        userDTO.setPassword(userBE.getPassword());
        userDTO.setEmail(userBE.getEmail());
        userDTO.setGender(userBE.getGender().getValue());
        userDTO.setDateOfBirth(userBE.getDateOfBirth());

        return userDTO;
    }
}
