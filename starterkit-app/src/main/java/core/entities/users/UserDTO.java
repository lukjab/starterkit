package core.entities.users;

import java.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;

public class UserDTO {

	Long id;
	String password;
	String email;
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	LocalDate dateOfBirth;
	Character gender;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Character getGender() {
		return gender;
	}
	public void setGender(Character gender) {
		this.gender = gender;
	}
	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(LocalDate dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
}
