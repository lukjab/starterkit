package core.entities.users.gender;


public enum Gender {
    MALE('M'),
    FEMALE('F');

    private Character value;

    public Character getValue() {
        return value;
    }

    Gender(Character value) {
        this.value = value;
    }

    public static Gender fromValue(char value) {
        for (Gender gender : Gender.values()) {
            if (gender.getValue().equals(value)) {
                return gender;
            }
        }
        throw new UnsupportedOperationException(
                "Kod  " + value + " nie jest obsługiwany!");
    }
}
