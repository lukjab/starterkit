package core.entities.users;

import static core.entities.users.UserBE.*;
import javax.persistence.*;
import java.time.LocalDate;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import core.entities.users.gender.Gender;
import core.entities.users.gender.GenderType;

@TypeDefs({@TypeDef(name = "GenderType", typeClass = GenderType.class)})
@Entity
@Table(name = "USERS", catalog = "public")
@NamedQueries({
    @NamedQuery(name = findUserByEmailQueryName, query = "SELECT usr FROM UserBE usr WHERE usr.email = :" + PARAM_EMAIL)
})
public class UserBE {
    public static final String findAllUsersSQL = "FROM UserBE";
    public static final String findUserByEmailQueryName = "findUserByEmail";
    public static final String PARAM_EMAIL = "email";
    
    @Id
    @GeneratedValue(generator = "users_id_seq")
    @SequenceGenerator(name = "users_id_seq", sequenceName = "users_id_seq", allocationSize = 1)
    @Column(name = "id")
    private Long id;

    @Column(name = "password")
    private String password;

    @Column(name = "email")
    private String email;

    @Column(name = "date_of_birth")
    private LocalDate dateOfBirth;

    @Column(name = "gender")
    @Type(type = "GenderType")
    private Gender gender;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String set_password) {
        this.password = set_password;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

}
