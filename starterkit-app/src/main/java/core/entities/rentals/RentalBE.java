package core.entities.rentals;

import core.entities.copies.CopyBE;
import core.entities.users.UserBE;

import javax.persistence.*;

import static core.entities.rentals.RentalBE.*;

@Entity
@Table(name = "RENTALS", catalog = "public")
@NamedQueries({
        @NamedQuery(name = findAllRentalsQueryName, query = "SELECT rent FROM RentalBE rent"),
        @NamedQuery(name = findRentalsByUserQueryName, query = "SELECT rent FROM RentalBE rent WHERE rent.user.id = :" + PARAM_USER_ID),
        @NamedQuery(name = findRentalByCopyQueryName, query = "SELECT rental FROM RentalBE rental WHERE rental.copy.id = :" + PARAM_COPY_ID)
})
public class RentalBE {
    public static final String findAllRentalsQueryName = "findAllRentals";
    public static final String findRentalsByUserQueryName = "findRentalsByUser";
    public static final String findRentalByCopyQueryName = "findRentalByCopy";

    public static final String PARAM_USER_ID = "userId";
    public static final String PARAM_COPY_ID = "copyId";

    @Id
    @GeneratedValue(generator = "rentals_id_seq")
    @SequenceGenerator(name = "rentals_id_seq", sequenceName = "rentals_id_seq", allocationSize = 1)
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "fk_user")
    private UserBE user;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "fk_copy")
    private CopyBE copy;

    @Transient
    public String getMovieTitle() {
        return this.copy.getMovieTitle();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserBE getUser() {
        return user;
    }

    public void setUser(UserBE user) {
        this.user = user;
    }

    public CopyBE getCopy() {
        return copy;
    }

    public void setCopy(CopyBE copy) {
        this.copy = copy;
    }
}
