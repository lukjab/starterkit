package core.entities.rentals;

public class RentalDTO {

    private Long id;
    private Long userId; //TODO ljablons - do rozważenia czy przesyłamy tylko id czy przesyłamy całe obiekty - do dyskusji
    private Long copyId;
    private String movieTitle;

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public Long getUserId() { return userId; }

    public void setUserId(Long userId) { this.userId = userId; }

    public Long getCopyId() { return copyId; }

    public void setCopyId(Long copyId) { this.copyId = copyId; }

    public String getMovieTitle() {
        return movieTitle;
    }

    public void setMovieTitle(String movieTitle) {
        this.movieTitle = movieTitle;
    }
}
