package core.entities.rentals;

import core.utilities.users.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.starterkit.controller.services.exceptions.UserNotFoundException;

@Component
public class RentalConverter {

    @Autowired
    private UserDAO userDAO;

    public RentalBE convertToBE(RentalDTO rentalDTO) throws UserNotFoundException {
        RentalBE rentalBE = new RentalBE();

        rentalBE.setId(rentalDTO.getId());
        rentalBE.setUser(userDAO.findById(rentalDTO.getUserId()));
        rentalBE.setCopy(null); //TODO dodać wyszukiwaną kopię jak już będzie podłączone CopyDAO

        return rentalBE;
    }

    public RentalDTO convertToDTO(RentalBE rentalBE) {
        RentalDTO rentalDTO = new RentalDTO();

        rentalDTO.setId(rentalBE.getId());
        rentalDTO.setUserId(rentalBE.getUser().getId());
        rentalDTO.setCopyId(rentalBE.getCopy().getId());
        rentalDTO.setMovieTitle(rentalBE.getMovieTitle());

        return rentalDTO;
    }

}
