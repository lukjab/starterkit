package com.starterkit.data;

public class GenresTestData {
    public static final int NUMBER_OF_ALL_GENRES = 5;

    public static Long HORROR_ID;
	public static Long THRILLER_ID;
	public static Long SCI_FI_ID;
	public static Long COMEDY_ID;
	public static Long ACTION_ID;

	public static final String HORROR_NAME = "HORROR";
	public static final String THRILLER_NAME = "THRILLER";
	public static final String SCI_FI_NAME = "SCI-FI";
	public static final String COMEDY_NAME = "KOMEDIA";
	public static final String ACTION_NAME = "AKCJA";

	public static final String HORROR_DESC = "Odmiana fantastyki polegająca na budowaniu świata aby wprowadzić zjawiska kwestionujące, nie dające się wytłumaczyć bez odwoływania się do zjawisk nadprzyrodzonych.";
	public static final String THRILLER_DESC = "Rodzaj filmu mający wywołać u widza dreszcz emocji. W odróżnieniu od horroru tajemniczość i niesamowitość w thrillerze nie mają jednak cech nadprzyrodzonych, zagrożenie jest realne.";
	public static final String SCI_FI_DESC = "Gatunek o fabule osnutej na przewidywanych osiągnięciach nauki i techniki oraz ukazującej ich wpływ na życie jednostki lub społeczeństwa.";
	public static final String COMEDY_DESC = "Film przedstawiający sytuacje i postacie wywołujące u widzów efekt komiczny.";
	public static final String ACTION_DESC = "Głównym zadaniem jest dostarczanie rozrywki widzom poprzez pokazywanie pościgów samochodowych, strzelanin, bijatyk i innych scen kaskaderskich o dużym ładunku napięcia i emocji.";

}
