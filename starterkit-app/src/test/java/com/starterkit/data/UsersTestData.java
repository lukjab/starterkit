package com.starterkit.data;

import core.entities.users.UserBE;

import java.util.ArrayList;

import static com.google.common.collect.Lists.newArrayList;

public class UsersTestData {
	public static final long NON_EXISTING_ID = 666L;
	public static UserBE USER_PUTIN;
	public static final String PUTIN_EMAIL = "putin@russia.com";
	public static final String PUTIN_NEW_EMAIL = "putinNew@russia.com";
	public static UserBE USER_TRUMP;
	public static final String TRUMP_EMAIL = "trump@usa.com";
	public static UserBE USER_MACRON;
	public static final String MACRON_EMAIL = "macron@france.com";
	public static final ArrayList<UserBE> DUMMY_USERS = newArrayList(new UserBE(), new UserBE());
	public static final String NON_EXISTING_EMAIL = "notExistingEmail@absent.com";
}
