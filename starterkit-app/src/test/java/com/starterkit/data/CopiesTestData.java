package com.starterkit.data;

import core.entities.copies.CopyBE;
import core.entities.movies.MovieBE;

public class CopiesTestData {
    public static Long COPY_1_ID;
    public static Long COPY_2_ID;
    public static CopyBE COPY_1;
    public static CopyBE COPY_2;
    public static MovieBE MOVIE_FOR_COPIES;

}
