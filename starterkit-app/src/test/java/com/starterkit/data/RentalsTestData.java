package com.starterkit.data;

import core.entities.copies.CopyBE;
import core.entities.rentals.RentalBE;
import core.entities.users.UserBE;

public class RentalsTestData {
    public static RentalBE RENTAL_WITH_USER_1;
    public static RentalBE RENTAL_WITH_USER_2;
    public static RentalBE RENTAL_WITHOUT_USER;
    public static Long RENTAL_WITH_USER_1_ID;

    public static CopyBE COPY_TO_RENTAL_1;
    public static CopyBE COPY_TO_RENTAL_2;
    public static Long COPY_TO_RENTAL_1_ID;

    public static UserBE RENTAL_USER;
    public static Long RENTAL_USER_ID;

}
