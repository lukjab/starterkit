package com.starterkit.data;

import java.time.LocalDate;

import core.entities.genres.GenreBE;
import core.entities.movies.MovieBE;

public class MoviesTestData {
    public static MovieBE SAMPLE_HORROR_MOVIE;
    public static MovieBE SAMPLE_COMEDY_MOVIE;
    public static GenreBE HORROR;
    public static GenreBE COMEDY;
    public static GenreBE ACTION;
    public static final String HORROR_MOVIE_TITLE = "Koszmar z ulicy wiązów";
    public static final String HORROR_NAME = "Horror";
    public static final String COMEDY_NAME = "Komedia";
    public static final String ACTION_NAME = "Akcja";
    public static LocalDate PREMIER_DATE;
    public static final Integer YEAR = 2018;
}

