package com.starterkit.configuration;

import javax.sql.DataSource;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableAutoConfiguration(exclude = { DataSourceAutoConfiguration.class,
		DataSourceTransactionManagerAutoConfiguration.class, HibernateJpaAutoConfiguration.class })
@ComponentScan(basePackages = { "core.utilities", "core.entities", "com.starterkit.controller.services" })
@EnableTransactionManagement
public class TestsInitializer {
	@Bean
	public LocalEntityManagerFactoryBean entityManagerFactory() {
		LocalEntityManagerFactoryBean factoryBean = new LocalEntityManagerFactoryBean();
		factoryBean.setPersistenceUnitName("testJPA");
		return factoryBean;
	}

	@Bean
	public DataSource dataSource() { //TODO prawdopodobnie usunąć
		return DataSourceBuilder
				.create()
				.url("jdbc:hsqldb:mem:butterfly;sql.syntax_pgs=true")
				.username("sa")
				.password("")
				.driverClassName("org.hsqldb.jdbcDriver")
				.build();
	}

	@Bean
	public PlatformTransactionManager transactionManager() {
		final PlatformTransactionManager transactionManager;
		final JpaTransactionManager jpaTransactionManager = new JpaTransactionManager();
		jpaTransactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
		//jpaTransactionManager.setDataSource(dataSource());
		transactionManager = jpaTransactionManager;
		return transactionManager;
	}
}
