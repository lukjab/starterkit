package com.starterkit.controller.services;

import static com.starterkit.data.GenresTestData.HORROR_DESC;
import static com.starterkit.data.GenresTestData.HORROR_NAME;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;
import com.starterkit.configuration.TestsInitializer;
import com.starterkit.controller.services.genres.GenresService;

import core.entities.genres.GenreBE;
import core.entities.genres.GenreDTO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestsInitializer.class}, loader = AnnotationConfigContextLoader.class)
@Transactional
public class GenresServiceIntegrationTest {
    @Autowired
    private GenresService genresService;
    @PersistenceContext
    private EntityManager testPersistenceUnitEntityManager;

    @Test
    public void shouldReturnEmptyListOfGenres() {
        List<GenreDTO> genresFromService = genresService.findAllGenres();
        assertTrue(genresFromService.isEmpty());
    }

    @Test
    public void shouldSaveDTOInDB() {
        //given
        GenreDTO horror = prepareHorrorDTO(null);
        //when
        genresService.saveGenre(horror);
        //then
        List<GenreBE> result = getAllGenresFromDB();
        assertTrue(!result.isEmpty());
        assertEquals(result.get(0).getName(), HORROR_NAME);
        assertEquals(result.get(0).getDescription(), HORROR_DESC);
    }

    @Test
    public void shouldUpdateRecordInDB() {
        //given
        GenreBE horrorBE = prepareHorrorBE();
        saveRecordInDB(horrorBE);
        GenreDTO genreDTO = prepareHorrorDTO(horrorBE.getId());

        String nameToReplace = "SAMPLE NAME";
        String descToReplace = "SAMPLE DESCRIPTION";
        genreDTO.setName(nameToReplace);
        genreDTO.setDescription(descToReplace);
        //when
        genresService.saveGenre(genreDTO);
        //then
        GenreBE result = getAllGenresFromDB().get(0);
        assertEquals(result.getName(), nameToReplace);
        assertEquals(result.getDescription(), descToReplace);
    }

    private GenreDTO prepareHorrorDTO(Long id) {
        GenreDTO horrorDTO = new GenreDTO();
        horrorDTO.setName(HORROR_NAME);
        horrorDTO.setDescription(HORROR_DESC);
        if (id != null) {
            horrorDTO.setId(id);
        }
        return horrorDTO;
    }

    private GenreBE prepareHorrorBE() {
        GenreBE horrorBE = new GenreBE();
        horrorBE.setName(HORROR_NAME);
        horrorBE.setDescription(HORROR_DESC);
        return horrorBE;
    }

    private List<GenreBE> getAllGenresFromDB() {
        return testPersistenceUnitEntityManager.createQuery(GenreBE.findAllGenresSQL, GenreBE.class).getResultList();
    }

    private void saveRecordInDB(GenreBE genreBE) {
        testPersistenceUnitEntityManager.persist(genreBE);
    }
}
