package com.starterkit.controller.services.rentals.validator;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import com.google.common.collect.Lists;
import com.starterkit.controller.services.exceptions.NoCopiesAvailableException;

import core.entities.copies.CopyBE;
import core.entities.rentals.RentalBE;
import core.utilities.rentals.RentalDAO;

@RunWith(MockitoJUnitRunner.class)
public class RentalValidatorTest {

    @Mock
    private RentalDAO rentalDAO;

    @InjectMocks
    private RentalValidator rentalValidator;

    @Test(expected = NoCopiesAvailableException.class)
    public void shouldThrowExceptionForEmptyList() throws NoCopiesAvailableException {
        rentalValidator.validateAvailableCopies(Lists.newArrayList());
    }

    @Test(expected = NoCopiesAvailableException.class)
    public void shouldThrowExceptionForAllRentedCopies() throws NoCopiesAvailableException {
        //given
        List<CopyBE> copies = prepareCopies();
        //when
        rentalValidator.validateAvailableCopies(copies);
    }

    private List<CopyBE> prepareCopies() {
        List<CopyBE> copies = new ArrayList<>();
        for (Long i = 0L; i < 5L; i++) {
            CopyBE copyBE = new CopyBE();
            copyBE.setId(i);
            when(rentalDAO.findRentalByCopy(i)).thenReturn(new RentalBE());
            copies.add(copyBE);
        }
        return copies;
    }

}