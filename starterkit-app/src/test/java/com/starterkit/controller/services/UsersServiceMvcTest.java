package com.starterkit.controller.services;

import com.starterkit.controller.services.users.UsersService;
import core.entities.users.UserConverter;
import core.entities.users.UserDTO;
import core.utilities.users.UserDAO;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static com.starterkit.data.UsersTestData.DUMMY_USERS;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class UsersServiceMvcTest {

    MockMvc mockMvc;

    @Mock
    private UserDAO userDAO;

    @Mock
    private UserConverter userConverter;

    @Before
    public void init() {
        initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(new UsersService(userDAO, userConverter)).build();
    }

    @Test
    public void shouldGetAllUsers() throws Exception {
        //given
        when(userDAO.findAllUsers()).thenReturn(DUMMY_USERS);
        when(userConverter.convertToDTO(any())).thenReturn(new UserDTO());
        //when && then
        mockMvc.perform(get("/api/users"))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print());
        verify(userDAO, times(1)).findAllUsers();
        verify(userConverter, times(2)).convertToDTO(any());
        noMoreInteractions();
    }

    private void noMoreInteractions() {
        verifyNoMoreInteractions(userDAO, userConverter);
    }

}
