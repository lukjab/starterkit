package core.utilities.rentals;

import static com.starterkit.data.RentalsTestData.*;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import java.util.ArrayList;
import java.util.List;
import com.google.common.collect.Lists;
import com.starterkit.configuration.TestsInitializer;
import core.entities.copies.CopyBE;
import core.entities.rentals.RentalBE;
import core.entities.users.UserBE;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestsInitializer.class}, loader = AnnotationConfigContextLoader.class)
@Transactional
public class RentalDAOIntegrationTest {
    @Autowired
    private RentalDAO rentalDAO;
    @PersistenceContext
    private EntityManager testPersistenceUnitEntityManager;

    private static final List<RentalBE> SAMPLE_RENTALS = new ArrayList<>();

    @Before
    public void prepareTestData() {
        //given
        SAMPLE_RENTALS.clear();

        RENTAL_WITH_USER_1 = new RentalBE();
        RENTAL_WITH_USER_2 = new RentalBE();
        RENTAL_WITHOUT_USER = new RentalBE();

        SAMPLE_RENTALS.add(RENTAL_WITH_USER_1);
        SAMPLE_RENTALS.add(RENTAL_WITH_USER_2);
        SAMPLE_RENTALS.add(RENTAL_WITHOUT_USER);

        saveSampleRentals(SAMPLE_RENTALS);
        saveCopiesForRentals();
        saveUserForRentals();
        prepareIDs();
    }

    private void saveSampleRentals(List<RentalBE> sampleRentalsToSave) {
        for (RentalBE rentalToSave : sampleRentalsToSave) {
            testPersistenceUnitEntityManager.persist(rentalToSave);
        }
    }

    private void saveCopiesForRentals() {
        COPY_TO_RENTAL_1 = new CopyBE();
        COPY_TO_RENTAL_2 = new CopyBE();
        testPersistenceUnitEntityManager.persist(COPY_TO_RENTAL_1);
        testPersistenceUnitEntityManager.persist(COPY_TO_RENTAL_2);
        RENTAL_WITH_USER_1.setCopy(COPY_TO_RENTAL_1);
        RENTAL_WITH_USER_2.setCopy(COPY_TO_RENTAL_2);
    }

    private void saveUserForRentals() {
        RENTAL_USER = new UserBE();
        testPersistenceUnitEntityManager.persist(RENTAL_USER);
        RENTAL_WITH_USER_1.setUser(RENTAL_USER);
        RENTAL_WITH_USER_2.setUser(RENTAL_USER);
    }

    private void prepareIDs() {
        COPY_TO_RENTAL_1_ID = COPY_TO_RENTAL_1.getId();
        RENTAL_USER_ID = RENTAL_USER.getId();
        RENTAL_WITH_USER_1_ID = RENTAL_WITH_USER_1.getId();
    }

    @Test
    public void shouldFindActiveRentalForCopy() {
        //when
        RentalBE result = rentalDAO.findRentalByCopy(COPY_TO_RENTAL_1_ID);
        //then
        assertEquals(RENTAL_WITH_USER_1, result);
    }

    @Test
    public void shouldFindAllRentalsByUser() {
        //when
        List<RentalBE> result = rentalDAO.findRentalsByUser(RENTAL_USER_ID);
        //then
        assertArrayEquals(Lists.newArrayList(RENTAL_WITH_USER_1, RENTAL_WITH_USER_2).toArray(), result.toArray());
    }

    @Test
    public void shouldFindAllRentals() {
        //when
        List<RentalBE> result = rentalDAO.findAllRentals();
        //then
        assertArrayEquals(Lists.newArrayList(RENTAL_WITH_USER_1, RENTAL_WITH_USER_2, RENTAL_WITHOUT_USER).toArray(), result.toArray());
    }

}
