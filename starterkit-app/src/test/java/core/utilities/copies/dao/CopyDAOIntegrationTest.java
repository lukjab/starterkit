package core.utilities.copies.dao;

import static com.starterkit.data.CopiesTestData.*;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;
import com.google.common.collect.Lists;
import com.starterkit.configuration.TestsInitializer;

import core.entities.copies.CopyBE;
import core.entities.copies.status.CopyStatus;
import core.entities.movies.MovieBE;
import core.entities.rentals.RentalBE;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestsInitializer.class}, loader = AnnotationConfigContextLoader.class)
@Transactional
public class CopyDAOIntegrationTest {
    @Autowired
    private CopyDAO copyDAO;
    @PersistenceContext
    private EntityManager testPersistenceUnitEntityManager;

    private static final List<CopyBE> SAMPLE_COPIES = new ArrayList<>();

    @Before
    public void saveCopies() {
        //given
        SAMPLE_COPIES.clear();

        COPY_1 = new CopyBE();
        COPY_1.setStatus(CopyStatus.AVAILABLE);
        COPY_2 = new CopyBE();
        COPY_2.setStatus(CopyStatus.UNAVAILABLE);

        SAMPLE_COPIES.add(COPY_1);
        SAMPLE_COPIES.add(COPY_2);
        saveSampleCopies(SAMPLE_COPIES);
        saveMovieForCopies();
    }

    private void saveMovieForCopies() {
        MOVIE_FOR_COPIES = new MovieBE();
        testPersistenceUnitEntityManager.persist(MOVIE_FOR_COPIES);
        COPY_1.setMovie(MOVIE_FOR_COPIES);
        COPY_2.setMovie(MOVIE_FOR_COPIES);
    }

    private void saveSampleCopies(List<CopyBE> sampleCopiesToSave) {
        for (CopyBE copyToSave : sampleCopiesToSave) {
            testPersistenceUnitEntityManager.persist(copyToSave);
        }
    }

    @Test
    public void shouldFindAllCopies() {
        //when
        List<CopyBE> result = copyDAO.findAllCopies();
        //then
        assertArrayEquals(Lists.newArrayList(COPY_1, COPY_2).toArray(), result.toArray());
    }

    @Test
    public void shouldFindAllCopiesByMovie() {
        //TODO napisać implementację
    }

    @Test
    public void shouldFindAllAvailableCopiesByMovie() {
        //given
        saveRental();
        //when
        List<CopyBE> result = copyDAO.findAllAvailableCopiesByMovie(MOVIE_FOR_COPIES.getId());
        //then
        assertEquals(Lists.newArrayList(COPY_2), result);
    }

    private void saveRental() {
        RentalBE rental_copy_1 = new RentalBE();
        rental_copy_1.setCopy(COPY_1);
        testPersistenceUnitEntityManager.persist(rental_copy_1);
    }

    @Test
    public void shouldFindAllAvailableCopies() {
        // when
        List<CopyBE> allAvailableCopies = copyDAO.findAllAvailableCopies();

        // then
        assertEquals(1, allAvailableCopies.size());
        assertEquals(COPY_1, allAvailableCopies.get(0));
    }
}
