package core.utilities.copies.bs;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import com.google.common.collect.Lists;

import core.entities.copies.CopyBE;
import core.entities.copies.status.CopyStatus;
import core.entities.movies.MovieBE;
import core.entities.rentals.RentalBE;
import core.utilities.copies.dao.CopyDAO;
import core.utilities.rentals.RentalDAO;

@RunWith(MockitoJUnitRunner.class)
public class CopiesFinderBSTest {
    public static final int NUMBER_OF_ALL_COPIES = 10;
    public static final int NUMBER_OF_ALL_AVAILABLE_COPIES = NUMBER_OF_ALL_COPIES / 2;
    public static final int NUMBER_OF_ALL_COPIES_WITH_EVEN_MOVIE_IDS = NUMBER_OF_ALL_COPIES / 2;
    public static final Long USER_ID = 1L;
    public static final Long MOVIE_ID = 0L;

    @InjectMocks
    private CopiesFinderBS copiesFinderBS;
    @Mock
    private CopyDAO copyDAO;
    @Mock
    private RentalDAO rentalDAO;

    @Test
    public void shouldFindAllCopies() {
        // given
        mockFindingCopies();
        // when
        List<CopyBE> result = copiesFinderBS.findAllCopies();

        // then
        assertEquals(NUMBER_OF_ALL_COPIES, result.size());
    }

    private void mockFindingCopies() {
        List<CopyBE> allCopies = prepareAllCopies();
        List<CopyBE> allAvailableCopies = allCopies.stream()
                .filter(copyBE -> CopyStatus.AVAILABLE.equals(copyBE.getStatus()))
                .collect(Collectors.toList());
        List<RentalBE> rentalsForUser = prepareRentalsForUser();
        List<CopyBE> copiesForMovieId = allCopies.stream()
                .filter(copyBE -> MOVIE_ID.equals(copyBE.getMovie().getId()))
                .collect(Collectors.toList());

        when(copyDAO.findAllCopies()).thenReturn(allCopies);
        when(copyDAO.findAllAvailableCopies()).thenReturn(allAvailableCopies);
        when(rentalDAO.findRentalsByUser(USER_ID)).thenReturn(rentalsForUser);
        when(copyDAO.findAllCopiesByMovie(MOVIE_ID)).thenReturn(copiesForMovieId);
    }

    private List<CopyBE> prepareAllCopies() {
        List<CopyBE> copies = new ArrayList<>();
        for (Long i = 0L; i < NUMBER_OF_ALL_COPIES; i++) {
            CopyBE newCopy = new CopyBE();
            MovieBE newMovie = new MovieBE();
            newMovie.setId(i % 2);
            newCopy.setId(i);
            newCopy.setMovie(newMovie);
            newCopy.setStatus(i % 2 == 0 ? CopyStatus.AVAILABLE : CopyStatus.UNAVAILABLE);
            copies.add(newCopy);
        }

        return copies;
    }

    private List<RentalBE> prepareRentalsForUser() {
        List<RentalBE> rentals = new ArrayList<>();

        rentals.add(new RentalBE());
        rentals.add(new RentalBE());
        Long copyId = 0L;
        for (RentalBE rentalForUser : rentals) {
            CopyBE copy = new CopyBE();
            MovieBE movie = new MovieBE();
            movie.setId(MOVIE_ID);
            copy.setMovie(movie);
            copy.setStatus(CopyStatus.UNAVAILABLE);
            copy.setId(copyId++);

            rentalForUser.setCopy(copy);
        }

        return rentals;
    }

    @Test
    public void shouldFindAllAvailableCopies() {
        // given
        mockFindingCopies();

        // when
        List<CopyBE> availableCopies = copiesFinderBS.findAllAvailableCopies();

        // then
        assertEquals(NUMBER_OF_ALL_AVAILABLE_COPIES, availableCopies.size());
    }

    @Test
    public void shouldFindAllRentedCopiesForUser() {
        // given
        mockFindingCopies();

        // when
        List<CopyBE> rentedCopiesForUser = copiesFinderBS.findAllRentedCopiesForUser(USER_ID);
        List<Long> rentedMovieIds = rentedCopiesForUser.stream()
                .map(copyBE -> copyBE.getMovie().getId())
                .collect(Collectors.toList());

        // then
        assertEquals(2, rentedCopiesForUser.size());
        assertArrayEquals(Lists.newArrayList(MOVIE_ID, MOVIE_ID).toArray(), rentedMovieIds.toArray());
    }

    @Test
    public void shouldFindAllCopiesForMovie() {
        // given
        mockFindingCopies();

        // when
        List<CopyBE> allCopiesForTitle = copiesFinderBS.findAllCopiesByMovie(MOVIE_ID);

        // then
        assertEquals(NUMBER_OF_ALL_COPIES_WITH_EVEN_MOVIE_IDS, allCopiesForTitle.size());
    }

}
