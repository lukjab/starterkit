package core.utilities.copies.bs;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import core.entities.copies.CopyBE;
import core.entities.copies.status.CopyStatus;
import core.utilities.copies.dao.CopyDAO;

@RunWith(MockitoJUnitRunner.class)
public class CopiesDataUpdateBSTest {

    private static final String EMPTY_DESCRIPTION = "";
    private static final String MISSING_DESCRIPTION = "Kopia zgubiona przez użytkownika";
    private static final String BROKEN_DESCRIPTION = "Kopia została porysowana.";

    @InjectMocks
    private CopiesDataUpdateBS copiesDataUpdateBS;
    @Mock
    private CopyDAO copyDAO;
    @Captor
    private ArgumentCaptor<CopyBE> copyCaptor;

    @Test
    public void shouldUpdateMissingStatusForCopy() {
        // given
        CopyBE copyToUpdate = prepareAvailableCopy();

        // when
        copiesDataUpdateBS.setMissingStatusWithDescription(copyToUpdate, MISSING_DESCRIPTION);

        // then
        verify(copyDAO, times(1)).updateCopy(copyCaptor.capture());
        assertEquals(CopyStatus.MISSING, copyCaptor.getValue().getStatus());
        assertEquals(MISSING_DESCRIPTION, copyCaptor.getValue().getDescription());
    }

    @Test
    public void shouldUpdateBrokenStatusForCopy() {
        // given
        CopyBE copyToUpdate = prepareAvailableCopy();

        // when
        copiesDataUpdateBS.setBrokenStatusWithDescription(copyToUpdate, BROKEN_DESCRIPTION);

        // then
        verify(copyDAO, times(1)).updateCopy(copyCaptor.capture());
        assertEquals(CopyStatus.BROKEN, copyCaptor.getValue().getStatus());
        assertEquals(BROKEN_DESCRIPTION, copyCaptor.getValue().getDescription());
    }

    private CopyBE prepareAvailableCopy() {
        CopyBE copy = new CopyBE();

        copy.setStatus(CopyStatus.AVAILABLE);
        copy.setDescription(EMPTY_DESCRIPTION);

        return copy;
    }
}
