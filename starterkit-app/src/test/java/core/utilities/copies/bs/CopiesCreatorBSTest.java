package core.utilities.copies.bs;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import core.entities.copies.CopyBE;
import core.entities.movies.MovieBE;
import core.utilities.copies.dao.CopyDAO;

@RunWith(MockitoJUnitRunner.class)
public class CopiesCreatorBSTest {

    public static final Long COPY_ID = 22L;
    public static final MovieBE SAMPLE_MOVIE = new MovieBE();
    public static final String EMPTY_DESCRIPTION = "";
    @InjectMocks
    private CopiesCreatorBS copiesCreatorBS;

    @Mock
    private CopyDAO copyDAO;
    @Captor
    private ArgumentCaptor<CopyBE> copyCaptor;

    @Test
    public void shouldCreateNewAvailableCopy() {
        // given
        CopyBE newCopyToCreate = prepareNewAvailableCopy();

        // when
        copiesCreatorBS.saveNewAvailableCopy(newCopyToCreate);

        // then
        verify(copyDAO, times(1)).saveNewCopy(copyCaptor.capture());
        assertEquals(COPY_ID, copyCaptor.getValue().getId());
        assertEquals(SAMPLE_MOVIE, copyCaptor.getValue().getMovie());
        assertEquals(EMPTY_DESCRIPTION, copyCaptor.getValue().getDescription());
    }

    private CopyBE prepareNewAvailableCopy() {
        CopyBE newAvailableCopy = new CopyBE();

        newAvailableCopy.setId(COPY_ID);
        newAvailableCopy.setMovie(SAMPLE_MOVIE);
        newAvailableCopy.setDescription(EMPTY_DESCRIPTION);

        return newAvailableCopy;
    }

}
