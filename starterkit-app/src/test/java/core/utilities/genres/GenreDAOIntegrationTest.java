package core.utilities.genres;

import static com.starterkit.data.GenresTestData.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;
import com.starterkit.configuration.TestsInitializer;

import core.entities.genres.GenreBE;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestsInitializer.class }, loader = AnnotationConfigContextLoader.class)
@Transactional
public class GenreDAOIntegrationTest {
	@Autowired
	private GenreDAO genreDAO;
	@PersistenceContext
	private EntityManager testPersistenceUnitEntityManager;

	private static final List<GenreBE> SAMPLE_GENRES = new ArrayList<>();

	@Before
	public void prepareTestData() {
		//given
		SAMPLE_GENRES.clear();

		GenreBE horror = new GenreBE();
		GenreBE thriller = new GenreBE();
		GenreBE sciFi = new GenreBE();
		GenreBE comedy = new GenreBE();
		GenreBE action = new GenreBE();

		horror.setName(HORROR_NAME);
		horror.setDescription(HORROR_DESC);

		thriller.setName(THRILLER_NAME);
		thriller.setDescription(THRILLER_DESC);

		sciFi.setName(SCI_FI_NAME);
		sciFi.setDescription(SCI_FI_DESC);

		comedy.setName(COMEDY_NAME);
		comedy.setDescription(COMEDY_DESC);

		action.setName(ACTION_NAME);
		action.setDescription(ACTION_DESC);

		SAMPLE_GENRES.add(horror);
		SAMPLE_GENRES.add(thriller);
		SAMPLE_GENRES.add(sciFi);
		SAMPLE_GENRES.add(comedy);
		SAMPLE_GENRES.add(action);

		saveSampleGenres(SAMPLE_GENRES);
		prepareIdsFromPersistedIds(SAMPLE_GENRES);

	}

	private void saveSampleGenres(List<GenreBE> sampleGenres) {
		for (GenreBE sampleGenreToSave : sampleGenres) {
			testPersistenceUnitEntityManager.persist(sampleGenreToSave);
		}
	}

	private void prepareIdsFromPersistedIds(List<GenreBE> sampleGenres) {
		for (GenreBE sampleGenre : sampleGenres) {
			switch (sampleGenre.getName()) {
			case HORROR_NAME:
				HORROR_ID = sampleGenre.getId();
				break;
			case THRILLER_NAME:
				THRILLER_ID = sampleGenre.getId();
				break;
			case SCI_FI_NAME:
				SCI_FI_ID = sampleGenre.getId();
				break;
			case COMEDY_NAME:
				COMEDY_ID = sampleGenre.getId();
				break;
			case ACTION_NAME:
				ACTION_ID = sampleGenre.getId();
				break;
			}
		}
	}

	@Test
	public void shouldReturnAllTestGenres() {
		//when
		List<GenreBE> allGenres = genreDAO.findAllGenres();
		//then
		assertEquals(NUMBER_OF_ALL_GENRES, allGenres.size());
	}

	@Test
	public void shouldReturnHorrorById() {
		//when
		GenreBE horrorFromDB = genreDAO.findById(HORROR_ID);
		//then
		assertEquals(HORROR_NAME, horrorFromDB.getName());
		assertEquals(HORROR_DESC, horrorFromDB.getDescription());
	}

	@Test
	public void shouldReturnThrillerById() {
		//when
		GenreBE thrillerFromDB = genreDAO.findById(THRILLER_ID);
		//then
		assertEquals(THRILLER_NAME, thrillerFromDB.getName());
		assertEquals(THRILLER_DESC, thrillerFromDB.getDescription());
	}

	@Test
	public void shouldReturnSciFiById() {
		//when
		GenreBE sciFiFromDB = genreDAO.findById(SCI_FI_ID);
		//then
		assertEquals(SCI_FI_NAME, sciFiFromDB.getName());
		assertEquals(SCI_FI_DESC, sciFiFromDB.getDescription());
	}

	@Test
	public void shouldReturnComedyById() {
		//when
		GenreBE comedyFromDB = genreDAO.findById(COMEDY_ID);
		//then
		assertEquals(COMEDY_NAME, comedyFromDB.getName());
		assertEquals(COMEDY_DESC, comedyFromDB.getDescription());
	}

	@Test
	public void shouldReturnActionById() {
		//when
		GenreBE actionFromDB = genreDAO.findById(ACTION_ID);
		//then
		assertEquals(ACTION_NAME, actionFromDB.getName());
		assertEquals(ACTION_DESC, actionFromDB.getDescription());
	}
	
	@Test
	public void shouldUpdateDescriptionForHorror() {
		//given
		String description = "SCARY MOVIE";
		changeGenreDescriptionForId(description, HORROR_ID);
		//when
		GenreBE horrorFromDB = genreDAO.findById(HORROR_ID);
		//then
		assertEquals(description, horrorFromDB.getDescription());
		assertNotEquals(HORROR_DESC, horrorFromDB.getDescription());
	}
	
	private void changeGenreDescriptionForId(String description, Long id) {
		testPersistenceUnitEntityManager.find(GenreBE.class, id).setDescription(description);
	}

}
