package core.utilities.users;

import static com.starterkit.data.UsersTestData.*;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.starterkit.controller.services.exceptions.UserNotFoundException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.starterkit.configuration.TestsInitializer;

import core.entities.users.UserBE;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestsInitializer.class }, loader = AnnotationConfigContextLoader.class)
@Transactional
public class UserDAOIntegrationTest {
	@Autowired
	private UserDAO userDAO;
	@PersistenceContext
	private EntityManager testPersistenceUnitEntityManager;

	private static final List<UserBE> SAMPLE_USERS = new ArrayList<>();

	@Before
	public void prepareTestUsers() {
		// given
		SAMPLE_USERS.clear();

		USER_TRUMP = new UserBE();
		USER_PUTIN = new UserBE();
		USER_MACRON = new UserBE();
		USER_TRUMP.setEmail(TRUMP_EMAIL);
		USER_PUTIN.setEmail(PUTIN_EMAIL);
		USER_MACRON.setEmail(MACRON_EMAIL);

		SAMPLE_USERS.add(USER_PUTIN);
		SAMPLE_USERS.add(USER_TRUMP);

		saveSampleUsers(SAMPLE_USERS);
	}

	private void saveSampleUsers(final List<UserBE> sampleUsers) {
		for (UserBE userToSave : sampleUsers) {
			testPersistenceUnitEntityManager.persist(userToSave);
		}
	}

	@Test
	public void shouldFindAllUsers() {
		// when
		List<UserBE> result = userDAO.findAllUsers();
		// then
		assertArrayEquals(Lists.newArrayList(USER_PUTIN, USER_TRUMP).toArray(), result.toArray());
	}

	@Test
	public void shouldFindUserByEmail() throws UserNotFoundException {
		// when
		UserBE result = userDAO.findByEmail(USER_TRUMP.getEmail());
		// then
		assertEquals(USER_TRUMP, result);
	}
	
	@Test(expected = UserNotFoundException.class)
	public void shouldThrowExceptionIfUserNotFoundByEmail() throws UserNotFoundException {
		// when
		userDAO.findByEmail(NON_EXISTING_EMAIL);
	}

	@Test
	public void shouldFindUserById() throws UserNotFoundException {
		// when
		UserBE result = userDAO.findById(USER_TRUMP.getId());
		// then
		assertEquals(USER_TRUMP, result);
	}

	@Test(expected = UserNotFoundException.class)
	public void shouldThrowExceptionIfUserNotFound() throws UserNotFoundException {
		// when
		userDAO.findById(NON_EXISTING_ID);
	}

	@Test
	public void shouldSaveNewUser() {
		// given
		List<UserBE> existingUsers = userDAO.findAllUsers();
		Optional<UserBE> existingMacron = getUserByEmail(existingUsers, MACRON_EMAIL);
		assertFalse(existingMacron.isPresent());
		// when
		userDAO.saveNewUser(USER_MACRON);
		// then
		existingUsers = userDAO.findAllUsers();
		Optional<UserBE> savedMacron = getUserByEmail(existingUsers, MACRON_EMAIL);
		assertTrue(savedMacron.isPresent());
	}

	private Optional<UserBE> getUserByEmail(List<UserBE> existingUsers, String email) {
		return existingUsers
				.stream()
				.filter(userFromDB -> userFromDB.getEmail().equals(email))
				.findFirst();
	}

	@Test
	public void shouldUpdateUser() throws UserNotFoundException {
		// given
		changeUserEmail(USER_PUTIN.getId(), PUTIN_NEW_EMAIL);
		userDAO.updateUser(USER_PUTIN);
		// when
		UserBE result = userDAO.findById(USER_PUTIN.getId());
		// then
		assertEquals(PUTIN_NEW_EMAIL, result.getEmail());
	}

	private void changeUserEmail(Long id, String newEmail) {
		testPersistenceUnitEntityManager.find(UserBE.class, id).setEmail(newEmail);
	}

	@Test
	public void shouldDeleteUser() {
		// when
		userDAO.deleteUser(USER_PUTIN.getId());
		// then
		List<UserBE> existingUsers = userDAO.findAllUsers();
		Optional<UserBE> existingPutin = getUserByEmail(existingUsers, PUTIN_EMAIL);
		assertFalse(existingPutin.isPresent());
	}

}
