package core.utilities.movies;

import static com.starterkit.data.MoviesTestData.ACTION;
import static com.starterkit.data.MoviesTestData.ACTION_NAME;
import static com.starterkit.data.MoviesTestData.COMEDY;
import static com.starterkit.data.MoviesTestData.COMEDY_NAME;
import static com.starterkit.data.MoviesTestData.HORROR;
import static com.starterkit.data.MoviesTestData.HORROR_MOVIE_TITLE;
import static com.starterkit.data.MoviesTestData.HORROR_NAME;
import static com.starterkit.data.MoviesTestData.PREMIER_DATE;
import static com.starterkit.data.MoviesTestData.SAMPLE_COMEDY_MOVIE;
import static com.starterkit.data.MoviesTestData.SAMPLE_HORROR_MOVIE;
import static com.starterkit.data.MoviesTestData.YEAR;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDate;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.starterkit.configuration.TestsInitializer;
import core.entities.genres.GenreBE;
import core.entities.movies.MovieBE;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestsInitializer.class}, loader = AnnotationConfigContextLoader.class)
@Transactional
public class MovieDAOIntegrationTest {
    @Autowired
    private MovieDAO movieDAO;

    @PersistenceContext
    private EntityManager testPersistenceUnitEntityManager;

    @Test
    public void shouldFindMovieByTitle() {
        //given
        prepareSampleHorrorMovie();
        //when
        MovieBE horrorByTitle = movieDAO.findByTitle(HORROR_MOVIE_TITLE);
        //then
        assertNotNull(horrorByTitle);
        assertEquals(HORROR_MOVIE_TITLE, horrorByTitle.getTitle());
    }

    @Test
    public void shouldFindMovieByHorrorGenre() {
        //given
        prepareHorrorGenre();
        prepareComedyGenre();
        prepareActionGenre();
        prepareSampleHorrorMovie();

        SAMPLE_HORROR_MOVIE.setGenres(Sets.newHashSet(HORROR, ACTION));
        //when
        List<MovieBE> result = movieDAO.findByGenres(Lists.newArrayList(HORROR.getId()));
        //then
        assertEquals(SAMPLE_HORROR_MOVIE, result.get(0));
    }

    @Test
    public void shouldFindMovieByActionGenre() {
        //given
        prepareHorrorGenre();
        prepareComedyGenre();
        prepareActionGenre();
        prepareSampleHorrorMovie();

        SAMPLE_HORROR_MOVIE.setGenres(Sets.newHashSet(HORROR, ACTION));
        //when
        List<MovieBE> result = movieDAO.findByGenres(Lists.newArrayList(ACTION.getId()));
        //then
        assertEquals(SAMPLE_HORROR_MOVIE, result.get(0));
    }

    @Test
    public void shouldFindMovieByYear() {
        //given
        prepareSampleHorrorMovie();
        //when
        List<MovieBE> result = movieDAO.findByYear(YEAR);
        //then
        assertEquals(SAMPLE_HORROR_MOVIE, result.get(0));
    }

    @Test
    public void shouldFindAllMovies() {
        //given
        prepareSampleHorrorMovie();
        prepareSampleComedyMovie();
        //when
        List<MovieBE> movies = movieDAO.findAllMovies();
        //then
        assertArrayEquals(Lists.newArrayList(SAMPLE_HORROR_MOVIE, SAMPLE_COMEDY_MOVIE).toArray(), movies.toArray());
    }

    private void prepareSampleHorrorMovie() {
        SAMPLE_HORROR_MOVIE = new MovieBE();
        SAMPLE_HORROR_MOVIE.setTitle(HORROR_MOVIE_TITLE);
        PREMIER_DATE = LocalDate.parse("2018-01-01");
        SAMPLE_HORROR_MOVIE.setPremierDate(PREMIER_DATE);
        testPersistenceUnitEntityManager.persist(SAMPLE_HORROR_MOVIE);
    }

    private void prepareSampleComedyMovie() {
        SAMPLE_COMEDY_MOVIE = new MovieBE();
        SAMPLE_COMEDY_MOVIE.setGenres(Sets.newHashSet(COMEDY));
        testPersistenceUnitEntityManager.persist(SAMPLE_COMEDY_MOVIE);
    }

    private void prepareHorrorGenre() {
        HORROR = new GenreBE();
        HORROR.setName(HORROR_NAME);
        testPersistenceUnitEntityManager.persist(HORROR);
    }

    private void prepareComedyGenre() {
        COMEDY = new GenreBE();
        COMEDY.setName(COMEDY_NAME);
        testPersistenceUnitEntityManager.persist(COMEDY);
    }

    private void prepareActionGenre() {
        ACTION = new GenreBE();
        ACTION.setName(ACTION_NAME);
        testPersistenceUnitEntityManager.persist(ACTION);
    }
}
