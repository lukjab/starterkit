import {MessageService} from "../services/message/message.service";
import {Injectable} from "@angular/core";

@Injectable()
export class ErrorHandler {
  constructor(private messageService: MessageService) {
  }

  public handleError(error: any): any {
    // TODO: zastąpić logowaniem do pliku jak na backendzie
    console.error(error);
    this.log(`Wystąpił błąd: ${error.message} -> ${error.error.message}; klasa wyjątku: ${error.error.exception}`);
    return error;
  }

  public log(message: string) {
    this.messageService.add('Operacja: ' + message);
  }
}
