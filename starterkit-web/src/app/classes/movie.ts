export class Movie {
  id: number;
  title:string;
  director:string;
  premierDate:string;
  description:string;
  numberOfAvailableCopies: number;
  posterFile: string;
}
