export class Rental {
  id: number;
  userId: number;
  copyId: number;
  movieTitle: string;
}
