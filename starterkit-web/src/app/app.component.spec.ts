import {async, TestBed} from '@angular/core/testing';
import {AppComponent} from './app.component';

import {MessagesComponent} from "./components/messages/messages.component";
import {CookieService} from "ngx-cookie-service";
import {RouterTestingModule} from "@angular/router/testing";
import {MessageService} from "./services/message/message.service";

describe('AppComponent', () => {
  const appTitle: string = 'Wypożyczalnia filmów Czarny Kot';

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        MessagesComponent
      ],
      providers: [
        CookieService,
        MessageService
      ],
      imports: [RouterTestingModule]
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;

    expect(app).toBeTruthy();
  }));

  it(`should have "${appTitle}" as title`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;

    expect(app.title).toEqual(appTitle);
  }));

  it('should render title in a h1 tag', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;

    expect(compiled.querySelector('h1').textContent).toContain(appTitle);
  }));
});
