import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {MovieDetailComponent} from "./movie-detail.component";
import {MatCardModule} from "@angular/material";
import {RouterTestingModule} from "@angular/router/testing";
import {MoviesServiceStub} from "../../services/movies/stub/movies.service.stub";
import {MoviesService} from "../../services/movies/movies.service";
import {RentalService} from "../../services/rental/rental.service";
import {RentalServiceStub} from "../../services/rental/stub/rental.service.stub";
import {CookieService} from "ngx-cookie-service";

describe('MovieDetailComponent', () => {
  let component: MovieDetailComponent;
  let fixture: ComponentFixture<MovieDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MovieDetailComponent],
      imports: [MatCardModule, RouterTestingModule],
      providers: [
        {provide: MoviesService, useClass: MoviesServiceStub},
        {provide: RentalService, useClass: RentalServiceStub},
        CookieService
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovieDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
