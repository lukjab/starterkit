import {Component, Input, OnInit} from "@angular/core";
import {Movie} from "../../classes/movie";
import {MoviesService} from "../../services/movies/movies.service";
import {ActivatedRoute} from "@angular/router";
import {Location} from "@angular/common";
import {RentalService} from "../../services/rental/rental.service";
import {CookieService} from "ngx-cookie-service";
import {SearchMoviesSessionStorageKeys} from "../searchmovies/sessionstorage/searchmovies-session-storage-keys";

@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.css']
})
export class MovieDetailComponent implements OnInit {

  @Input() movie: Movie;

  private waitingForResponse: boolean = false;

  constructor(private route: ActivatedRoute,
              private moviesService: MoviesService,
              private location: Location,
              private rentalService: RentalService,
              private cookieService: CookieService) {
  }

  ngOnInit() {
    this.findMovie();
  }

  findMovie(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.moviesService.findMovieById(id)
      .subscribe(movie => this.movie = movie);
  }

  goBack(): void {
    sessionStorage.setItem(SearchMoviesSessionStorageKeys.shouldLoadPreviouslyFoundMoviesKey, JSON.stringify(true));
    this.location.back();
  }

  isRentalDisabled(): boolean {
    return !this.waitingForResponse && this.movie.numberOfAvailableCopies === 0;
  }

  rentMovie(): void {
    this.waitingForResponse = true;

    const userId: number = Number(this.cookieService.get('userId'));
    this.rentalService.rentMovie(userId, this.movie.id)
      .subscribe(response => {
        this.movie.numberOfAvailableCopies--;
        this.waitingForResponse = false;
      });
  }

}
