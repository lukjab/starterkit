import {Component, Input, OnInit} from "@angular/core";
import {Movie} from "../../classes/movie";

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit {

  private _movies: Movie[];

  public get movies(): Movie[] {
    return this._movies;
  }

  @Input()
  public set movies(movies: Movie[]) {
    this._movies = movies;
  }

  constructor() {
  }

  ngOnInit() {
  }

}
