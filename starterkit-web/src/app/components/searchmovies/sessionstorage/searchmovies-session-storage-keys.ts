export class SearchMoviesSessionStorageKeys {
  static readonly selectedTabKey: string = 'selectedTabIndex';
  static readonly foundMoviesKey: string = 'foundMovies';
  static readonly shouldLoadPreviouslyFoundMoviesKey: string = 'shouldReadFoundMovies';
  static readonly searchedTitleKey: string = 'searchedTitle';
  static readonly selectedYearKey: string = 'selectedYear';
  static readonly selectedGenresKey: string = 'selectedGenres';

  static isValueSet(value: string) : boolean {
    return (value.toLowerCase() !== 'undefined') && (value.toLowerCase() !== 'null');
  }
}
