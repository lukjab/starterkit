import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SearchMoviesComponent} from './searchmovies.component';
import {MatCardModule, MatInputModule, MatSelectModule, MatTabsModule} from "@angular/material";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MoviesComponent} from "../movies/movies.component";
import {RouterTestingModule} from "@angular/router/testing";
import {SearchMoviesService} from "../../services/searchmovies/searchmovies.service";
import {SearchMoviesServiceStub} from "../../services/searchmovies/stub/searchmovies.service.stub";
import {GenresDataStoreService} from "../../services/genres/datastore/genres-data-store.service";
import {GenresDataStoreServiceStub} from "../../services/genres/datastore/stub/genres-data-store-service-stub";
import {MoviesService} from "../../services/movies/movies.service";
import {MoviesServiceStub} from "../../services/movies/stub/movies.service.stub";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";

describe('SearchMoviesComponent', () => {
  let component: SearchMoviesComponent;
  let fixture: ComponentFixture<SearchMoviesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SearchMoviesComponent, MoviesComponent],
      imports: [MatTabsModule, FormsModule, MatInputModule, MatSelectModule, ReactiveFormsModule, MatCardModule, RouterTestingModule, BrowserAnimationsModule],
      providers: [
        {provide: SearchMoviesService, useClass: SearchMoviesServiceStub},
        {provide: GenresDataStoreService, useClass: GenresDataStoreServiceStub},
        {provide: MoviesService, useClass: MoviesServiceStub}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchMoviesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
