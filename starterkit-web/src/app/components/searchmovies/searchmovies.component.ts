import {Component, OnInit} from "@angular/core";
import {SearchMoviesService} from "../../services/searchmovies/searchmovies.service";
import {Movie} from "../../classes/movie";
import {FormControl} from "@angular/forms";
import {MoviesService} from "../../services/movies/movies.service";
import {GenresDataStoreService} from "../../services/genres/datastore/genres-data-store.service";
import {SearchMoviesSessionStorageKeys} from "./sessionstorage/searchmovies-session-storage-keys";

@Component({
  selector: 'app-searchmovies',
  templateUrl: './searchmovies.component.html',
  styleUrls: ['./searchmovies.component.css']
})
export class SearchMoviesComponent implements OnInit {

  private readonly titleTabName: string = 'Tytuł';
  private readonly genresTabName: string = 'Gatunki';
  private readonly premierDateYearTabName: string = 'Rok premiery';

  private selectedTabIndex: number;

  private foundMovies: Movie[];
  private searchedTitle: string;
  private selectedGenres: FormControl = new FormControl();
  private selectedYear: FormControl = new FormControl();
  private premierDateYears: number[];

  private shouldShowErrorMessage: boolean = false;
  private errorMessage: string;

  findByTitle(title: string): void {
    this.clearTabs();
    if (title) {
      this.searchMoviesService.findMovieByTitle(title)
        .subscribe(movies => {
            if (movies) {
              this.shouldShowErrorMessage = false;
              this.foundMovies = movies;
              this.storeSearchState();
            }
          },
          error => {
            this.errorMessage = error.error.message;
            this.shouldShowErrorMessage = true;
          }
        );
    }
  }

  findByGenres(): void {
    this.clearTabs();
    const selectedGenresIds: number[] = this.selectedGenres.value;
    this.searchMoviesService.findMoviesByGenres(selectedGenresIds)
      .subscribe(movies => {
        this.foundMovies = movies;
        this.storeSearchState();
      });
  }


  findByYear(): void {
    this.clearTabs();
    const selectedYear: number = this.selectedYear.value;
    this.searchMoviesService.findMoviesByYear(selectedYear)
      .subscribe(movies => {
        this.foundMovies = movies;
        this.storeSearchState();
      });
  }

  clearTabs(): void {
    this.foundMovies = null;
    this.shouldShowErrorMessage = false;
  }

  manageTabsData(selectedTabIndex: number) {
    this.selectedTabIndex = selectedTabIndex;
    this.clearTabs();
    sessionStorage.setItem(SearchMoviesSessionStorageKeys.selectedTabKey, JSON.stringify(this.selectedTabIndex));
  }

  storeSearchState(): void {
    sessionStorage.setItem(SearchMoviesSessionStorageKeys.foundMoviesKey, JSON.stringify(this.foundMovies));
    sessionStorage.setItem(SearchMoviesSessionStorageKeys.searchedTitleKey, JSON.stringify(this.searchedTitle));
    sessionStorage.setItem(SearchMoviesSessionStorageKeys.selectedYearKey, JSON.stringify(this.selectedYear.value));
    sessionStorage.setItem(SearchMoviesSessionStorageKeys.selectedGenresKey, JSON.stringify(this.selectedGenres.value));
  }

  restoreSearchState(): void {
    const searchedTitle = sessionStorage.getItem(SearchMoviesSessionStorageKeys.searchedTitleKey),
      selectedYear = sessionStorage.getItem(SearchMoviesSessionStorageKeys.selectedYearKey),
      selectedGenres = sessionStorage.getItem(SearchMoviesSessionStorageKeys.selectedGenresKey);

    this.foundMovies = JSON.parse(sessionStorage.getItem(SearchMoviesSessionStorageKeys.foundMoviesKey));
    if (SearchMoviesSessionStorageKeys.isValueSet(searchedTitle)) {
      this.searchedTitle = JSON.parse(searchedTitle);
    }
    if (SearchMoviesSessionStorageKeys.isValueSet(selectedYear)) {
      this.selectedYear.setValue(JSON.parse(selectedYear));
    }
    if (SearchMoviesSessionStorageKeys.isValueSet(selectedGenres)) {
      this.selectedGenres.setValue(JSON.parse(selectedGenres));
    }
  }

  constructor(private searchMoviesService: SearchMoviesService,
              private genresDataStore: GenresDataStoreService,
              private moviesService: MoviesService) {
  }

  ngOnInit() {
    this.genresDataStore.loadAllAvailableGenres();
    this.moviesService.findAllPremierDateYears()
      .subscribe(
        years => this.premierDateYears = years
      );
    this.selectedTabIndex = JSON.parse(sessionStorage.getItem(SearchMoviesSessionStorageKeys.selectedTabKey));
    if (JSON.parse(sessionStorage.getItem(SearchMoviesSessionStorageKeys.shouldLoadPreviouslyFoundMoviesKey))) {
      this.restoreSearchState();
      sessionStorage.setItem(SearchMoviesSessionStorageKeys.shouldLoadPreviouslyFoundMoviesKey, JSON.stringify(false));
    }
  }
}
