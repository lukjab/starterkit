import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {UsersComponent} from './users.component';
import {RouterTestingModule} from "@angular/router/testing";
import {MatCardModule} from "@angular/material";
import {UsersDataStoreService} from "../../../services/user/datastore/users-data-store.service";
import {UsersDataStoreServiceStub} from "../../../services/user/datastore/stub/users-data-store.service.stub";

describe('UsersComponent', () => {
  let component: UsersComponent;
  let fixture: ComponentFixture<UsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UsersComponent],
      imports: [RouterTestingModule, MatCardModule],
      providers: [
        {provide: UsersDataStoreService, useClass: UsersDataStoreServiceStub}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
