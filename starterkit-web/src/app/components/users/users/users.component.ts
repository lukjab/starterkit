import {Component, OnInit} from '@angular/core';
import {UsersDataStoreService} from "../../../services/user/datastore/users-data-store.service";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  constructor(private usersDataStore: UsersDataStoreService) {
  }

  ngOnInit() {
    this.usersDataStore.loadAllUsers();
  }

}
