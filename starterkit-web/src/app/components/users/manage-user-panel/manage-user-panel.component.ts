import {Component, OnInit} from '@angular/core';
import {User} from "../../../classes/user";
import {UsersDataStoreService} from "../../../services/user/datastore/users-data-store.service";
import {ActivatedRoute} from "@angular/router";
import {RentalService} from "../../../services/rental/rental.service";
import {Rental} from "../../../classes/rental";
import {MatSelectionList} from "@angular/material";

@Component({
  selector: 'app-manage-user-panel',
  templateUrl: './manage-user-panel.component.html',
  styleUrls: ['./manage-user-panel.component.css']
})
export class ManageUserPanelComponent implements OnInit {
  private user: User;
  private userRentals: Rental[];

  constructor(private rentalService: RentalService,
              private usersDataStore: UsersDataStoreService,
              private route: ActivatedRoute) {
  }

  returnSelectedMovies(rentalsList: MatSelectionList): void {
    const rentedMoviesIds = rentalsList.selectedOptions.selected.map(item => item.value);
    this.rentalService.returnRentals(rentedMoviesIds)
      .subscribe(() => this.findUserRentals());
  }

  ngOnInit() {
    this.findUser();
    this.findUserRentals();
  }

  private findUser(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.usersDataStore.findUserById(id)
      .subscribe(user => this.user = user);
  }

  private findUserRentals(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.rentalService.findRentalsByUser(id)
      .subscribe(rentals => {
        this.userRentals = rentals;
      });
  }
}

