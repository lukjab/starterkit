import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ManageUserPanelComponent} from './manage-user-panel.component';
import {MatCardModule, MatListModule} from "@angular/material";
import {RentalService} from "../../../services/rental/rental.service";
import {RentalServiceStub} from "../../../services/rental/stub/rental.service.stub";
import {UsersDataStoreService} from "../../../services/user/datastore/users-data-store.service";
import {UsersDataStoreServiceStub} from "../../../services/user/datastore/stub/users-data-store.service.stub";
import {RouterTestingModule} from "@angular/router/testing";

describe('ManageUserPanelComponent', () => {
  let component: ManageUserPanelComponent;
  let fixture: ComponentFixture<ManageUserPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ManageUserPanelComponent],
      imports: [MatCardModule, MatListModule, RouterTestingModule],
      providers: [
        {provide: RentalService, useClass: RentalServiceStub},
        {provide: UsersDataStoreService, useClass: UsersDataStoreServiceStub}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageUserPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
