import {Component, Input, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {User} from '../../../classes/user';
import {UserService} from '../../../services/user/user.service';
import {ActivatedRoute, Router} from '@angular/router';
import {UsersDataStoreService} from "../../../services/user/datastore/users-data-store.service";

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  private errorMessage: string;
  private shouldShowErrorMessage: boolean = false;
  private shouldShowMessage: boolean = false;

  @Input() user: User = new User();

  constructor(private route: ActivatedRoute,
              private location: Location,
              private router: Router,
              private userService: UserService,
              private usersDataStore: UsersDataStoreService) {
  }

  ngOnInit() {
    this.user.gender = 'F';
    this.user.email = "";
  }

  goBack(): void {
    this.location.back();
  }

  saveUser(): void {
    this.userService.saveUser(this.user)
      .subscribe((response) => {
          this.usersDataStore.reloadAllUsers(), 
          this.shouldShowErrorMessage = false;
          this.shouldShowMessage = true;
      },
      error => {
          this.errorMessage = error.error.message;
          this.shouldShowErrorMessage = true;
          this.shouldShowMessage = false;
        }
      
      );
  }

  onGenderChange(gender): void {
    this.user.gender = gender;
  }

}
