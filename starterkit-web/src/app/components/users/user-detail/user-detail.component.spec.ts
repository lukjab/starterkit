import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {UserDetailComponent} from './user-detail.component';
import {RouterTestingModule} from "@angular/router/testing";
import {MatCardModule} from "@angular/material";
import {UsersDataStoreService} from "../../../services/user/datastore/users-data-store.service";
import {UsersDataStoreServiceStub} from "../../../services/user/datastore/stub/users-data-store.service.stub";

describe('UserDetailComponent', () => {
  let component: UserDetailComponent;
  let fixture: ComponentFixture<UserDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UserDetailComponent],
      imports: [RouterTestingModule, MatCardModule],
      providers: [
        {provide: UsersDataStoreService, useClass: UsersDataStoreServiceStub}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
