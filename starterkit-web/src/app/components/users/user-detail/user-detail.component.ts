import {Component, OnInit} from '@angular/core';
import {User} from "../../../classes/user";
import {Location} from "@angular/common";
import {ActivatedRoute} from "@angular/router";
import {UsersDataStoreService} from "../../../services/user/datastore/users-data-store.service";

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {
  private user: User;

  constructor(private route: ActivatedRoute,
              private usersDataStore: UsersDataStoreService,
              private location: Location) {
  }

  ngOnInit() {
    this.findUser();
  }

  findUser(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.usersDataStore.findUserById(id)
      .subscribe(user => this.user = user);
  }

  goBack(): void {
    this.location.back();
  }

}
