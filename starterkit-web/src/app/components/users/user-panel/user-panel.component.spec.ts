import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {UserPanelComponent} from './user-panel.component';
import {MatCardModule} from "@angular/material";
import {UserService} from "../../../services/user/user.service";
import {UserServiceStub} from "../../../services/user/stub/user.service.stub";
import {RentalService} from "../../../services/rental/rental.service";
import {RentalServiceStub} from "../../../services/rental/stub/rental.service.stub";
import {CookieService} from "ngx-cookie-service";

describe('UserPanelComponent', () => {
  let component: UserPanelComponent;
  let fixture: ComponentFixture<UserPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UserPanelComponent],
      imports: [MatCardModule],
      providers: [
        {provide: UserService, useClass: UserServiceStub},
        {provide: RentalService, useClass: RentalServiceStub},
        CookieService
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
