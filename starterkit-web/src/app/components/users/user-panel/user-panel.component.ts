import {Component, OnInit} from '@angular/core';
import {UserService} from "../../../services/user/user.service";
import {User} from "../../../classes/user";
import {CookieService} from "ngx-cookie-service";
import {RentalService} from "../../../services/rental/rental.service";
import {Rental} from "../../../classes/rental";

@Component({
  selector: 'app-user-panel',
  templateUrl: './user-panel.component.html',
  styleUrls: ['./user-panel.component.css']
})
export class UserPanelComponent implements OnInit {

  private loggedUser: User;
  private loggedUserRentals: Rental[];

  constructor(private userService: UserService,
              private cookieService: CookieService,
              private rentalService: RentalService) {
  }

  ngOnInit() {
    this.findLoggerdUser();
    this.findLoggedUserRentals();
  }

  private findLoggerdUser(): void {
    this.userService.findUserById(Number.parseInt(this.cookieService.get('userId')))
      .subscribe(user => this.loggedUser = user);
  }

  private findLoggedUserRentals(): void {
    this.rentalService.findRentalsByUser(Number.parseInt(this.cookieService.get('userId')))
      .subscribe(rentals => this.loggedUserRentals = rentals);
  }

}
