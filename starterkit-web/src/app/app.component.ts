import {Component} from "@angular/core";
import {CookieService} from "ngx-cookie-service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Wypożyczalnia filmów Czarny Kot';

  constructor(private cookieService: CookieService) {
    sessionStorage.clear(); //czyszczenie zapisanych danych przy przeladowniu strony
    this.cookieService.set('userId', '6'); //domyslny uzytkownik Admin
  }
}
