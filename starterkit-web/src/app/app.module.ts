import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {AppComponent} from "./app.component";
import {UsersComponent} from "./components/users/users/users.component";
import {UserService} from "./services/user/user.service";
import {UserDetailComponent} from "./components/users/user-detail/user-detail.component";
import {MessagesComponent} from "./components/messages/messages.component";
import {MessageService} from "./services/message/message.service";
import {AppRoutingModule} from "./modules/app-routing/app-routing.module";
import {DashboardComponent} from "./components/dashboard/dashboard.component";
import {ErrorHandler} from "./classes/error-handler";
import {SearchMoviesComponent} from "./components/searchmovies/searchmovies.component";
import {MatTabsModule} from "@angular/material/tabs";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {MatCardModule} from "@angular/material/card";
import {SearchMoviesService} from "./services/searchmovies/searchmovies.service";
import {GenresService} from "./services/genres/genres.service";
import {
  MatCheckboxModule, MatDatepickerModule, MatInputModule, MatListModule, MatNativeDateModule,
  MatSelectModule
} from "@angular/material";
import {MoviesService} from "./services/movies/movies.service";
import {MoviesComponent} from "./components/movies/movies.component";
import {MovieDetailComponent} from "./components/movie-detail/movie-detail.component";
import {RentalService} from "./services/rental/rental.service";
import {MatDialogModule} from "@angular/material/dialog";
import {CookieService} from "ngx-cookie-service";
import {GenresDataStoreService} from "./services/genres/datastore/genres-data-store.service";
import {AddUserComponent} from './components/users/add-user/add-user.component';
import {UserPanelComponent} from './components/users/user-panel/user-panel.component';
import {ManageUserPanelComponent} from './components/users/manage-user-panel/manage-user-panel.component';
import {UsersDataStoreService} from "./services/user/datastore/users-data-store.service";


@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    UserDetailComponent,
    MessagesComponent,
    DashboardComponent,
    SearchMoviesComponent,
    MoviesComponent,
    MovieDetailComponent,
    AddUserComponent,
    UserPanelComponent,
    ManageUserPanelComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    MatTabsModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatInputModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatDialogModule,
    MatCheckboxModule,
    MatListModule
  ],
  providers: [UserService, MessageService, ErrorHandler, SearchMoviesService, GenresService, MoviesService,
    RentalService, CookieService, GenresDataStoreService, UsersDataStoreService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
