import { TestBed, inject } from '@angular/core/testing';

import { GenresDataStoreService } from './genres-data-store.service';
import {GenresService} from "../genres.service";
import {GenresServiceStub} from "../stub/genres-service.stub";

describe('GenresDataStoreService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GenresDataStoreService,
        {provide: GenresService, useClass: GenresServiceStub}]
    });
  });

  it('should be created', inject([GenresDataStoreService], (service: GenresDataStoreService) => {
    expect(service).toBeTruthy();
  }));
});
