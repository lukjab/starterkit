import {Injectable} from '@angular/core';
import {GenresService} from "../genres.service";
import {Genre} from "../../../classes/genre";

@Injectable()
export class GenresDataStoreService {

  private _allAvailableGenres: Genre[] = [];

  get allAvailableGenres(): Genre[] {
    return this._allAvailableGenres;
  }

  set allAvailableGenres(value: Genre[]) {
    this._allAvailableGenres = value;
  }

  loadAllAvailableGenres(): void {
    if (this._allAvailableGenres.length === 0) {
      this.genresService.findAllGenres()
        .subscribe(genres => this._allAvailableGenres = genres);
    }
  }

  reloadAllAvailableGenres(): void {
    this.genresService.findAllGenres()
      .subscribe(genres => this._allAvailableGenres = genres);
  }

  constructor(private genresService: GenresService) {
  }

}
