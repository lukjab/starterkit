import {TestBed, inject} from '@angular/core/testing';

import {GenresService} from './genres.service';
import {HttpClient, HttpHandler} from "@angular/common/http";
import {MessageService} from "../message/message.service";
import {ErrorHandler} from "../../classes/error-handler";

describe('GenresService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GenresService, HttpClient, HttpHandler, MessageService, ErrorHandler]
    });
  });

  it('should be created', inject([GenresService], (service: GenresService) => {
    expect(service).toBeTruthy();
  }));
});
