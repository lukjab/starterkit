import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {ErrorHandler} from "../../classes/error-handler";
import {MessageService} from "../message/message.service";
import {Observable} from "rxjs/Observable";
import {Genre} from "../../classes/genre";
import {catchError, tap} from "rxjs/operators";

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable()
export class GenresService {
  private findAllGenresUrl: string = '/api/genres';

  findAllGenres(): Observable<Genre[]> {
    this.messageService.add('GenresService: żądanie findAllGenres wysłane pod adres: ' + this.findAllGenresUrl);
    return this.http.get<Genre[]>(this.findAllGenresUrl)
      .pipe(
        tap(genres => this.errorHandler.log('Przetworzono listę gatunków')),
        catchError((error: any) => Observable.throw(this.errorHandler.handleError(error)))
      );
  }

  constructor(private http: HttpClient,
              private messageService: MessageService,
              private errorHandler: ErrorHandler) {
  }

}
