import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {MessageService} from "../message/message.service";
import {ErrorHandler} from "../../classes/error-handler";
import {catchError, tap} from "rxjs/operators";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/observable/throw';
import {Movie} from "../../classes/movie";

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable()
export class SearchMoviesService {
  private findMovieByTitleUrl: string = '/api/searchmovies/title';
  private findMoviesByGenresUrl: string = '/api/searchmovies/genres';
  private findMoviesByYearUrl: string = '/api/searchmovies/year';

  constructor(private http: HttpClient,
              private messageService: MessageService,
              private errorHandler: ErrorHandler) {
  }

  findMovieByTitle(title: string): Observable<Movie[]> {
    this.messageService.add('SearchMoviesService: żądanie findMovieByTitle wysłane pod adres:' + this.findMoviesByYearUrl);
    return this.http.get<Movie[]>(this.findMovieByTitleUrl, {
      params: {
        title: title
      }
    })
      .pipe(
        tap(movie => this.errorHandler.log(`Przetworzono film dla tytułu ${title}`)),
        catchError((error: any) => Observable.throw(this.errorHandler.handleError(error)))
      );
  }




  findMoviesByGenres(genreIds: number[]): Observable<Movie[]> {
    this.messageService.add('SearchMoviesService: żądanie findMoviesByGenres wysłane pod adres:' + this.findMoviesByGenresUrl);
    return this.http.post<Movie[]>(this.findMoviesByGenresUrl, genreIds)
      .pipe(
        tap(users => this.errorHandler.log('Przetworzono listę filmów dla wybranych gatunków')),
        catchError((error: any) => Observable.throw(this.errorHandler.handleError(error)))
      );
  }

  findMoviesByYear(premierDateYear: number): Observable<Movie[]> {
    this.messageService.add('SearchMoviesService: żądanie findMoviesByYear wysłane pod adres:' + this.findMoviesByYearUrl);
    return this.http.get<Movie[]>(this.findMoviesByYearUrl, {
      params: {
        year: JSON.stringify(premierDateYear)
      }
    })
      .pipe(
        tap(users => this.errorHandler.log(`Przetworzono listę filmów dla roku ${premierDateYear}`)),
        catchError((error: any) => Observable.throw(this.errorHandler.handleError(error)))
      );
  }

}
