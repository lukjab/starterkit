import {TestBed, inject} from '@angular/core/testing';

import {SearchMoviesService} from './searchmovies.service';
import {HttpClient, HttpHandler} from "@angular/common/http";
import {MessageService} from "../message/message.service";
import {ErrorHandler} from "../../classes/error-handler";

describe('SearchMoviesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SearchMoviesService, HttpClient, HttpHandler, MessageService, ErrorHandler]
    });
  });

  it('should be created', inject([SearchMoviesService], (service: SearchMoviesService) => {
    expect(service).toBeTruthy();
  }));
});
