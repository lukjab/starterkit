import {Injectable} from "@angular/core";
import {MessageService} from "../message/message.service";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {ErrorHandler} from "../../classes/error-handler";
import {Observable} from "rxjs/Observable";
import {catchError, tap} from "rxjs/operators";
import {Movie} from "../../classes/movie";

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable()
export class MoviesService {

  private findAllPremierDateYearsUrl: string = "/api/movies/premierDateYears";
  private findMovieByIdUrl: string = "/api/movies/movieInfo";

  constructor(private http: HttpClient,
              private messageService: MessageService,
              private errorHandler: ErrorHandler) {
  }

  findAllPremierDateYears(): Observable<number[]> {
    this.messageService.add('MoviesService: żądanie findAllPremierDateYears wysłane pod adres: ' + this.findAllPremierDateYearsUrl);
    return this.http.get<number[]>(this.findAllPremierDateYearsUrl)
      .pipe(
        tap(years => this.errorHandler.log('Przetworzono lata premiery')),
        catchError((error: any) => Observable.throw(this.errorHandler.handleError(error)))
      )
  }

  findMovieById(id: number): Observable<Movie> {
    this.messageService.add(`MoviesService: żądanie findMovieById dla id=${id}`);
    return this.http.get<Movie>(this.findMovieByIdUrl, {
      params: {
        movieId: JSON.stringify(id)
      }
    })
      .pipe(
        tap(user => this.errorHandler.log(`Przetworzony film o id=${id}`)),
        catchError((error: any) => Observable.throw(this.errorHandler.handleError(error)))
      );
  }

}
