import {inject, TestBed} from '@angular/core/testing';

import {MoviesService} from './movies.service';
import {HttpClient, HttpHandler} from "@angular/common/http";
import {MessageService} from "../message/message.service";
import {ErrorHandler} from "../../classes/error-handler";

describe('MoviesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MoviesService, HttpClient, HttpHandler, MessageService, ErrorHandler]
    });
  });

  it('should be created', inject([MoviesService], (service: MoviesService) => {
    expect(service).toBeTruthy();
  }));
});
