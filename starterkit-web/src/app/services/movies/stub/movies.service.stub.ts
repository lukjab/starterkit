import {Observable} from "rxjs";
import {Movie} from "../../../classes/movie";

export class MoviesServiceStub {

  findMovieById(id: number): Observable<Movie> {
    const emptyMovie = new Movie;
    return Observable.of(emptyMovie);
  }

  findAllPremierDateYears(): Observable<number[]> {
    const sampleTestPremierDateYears = [1991, 2002, 2012, 2018];
    return Observable.of(sampleTestPremierDateYears);
  }
}
