import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {catchError, tap} from "rxjs/operators";
import {User} from "../../classes/user";
import {MessageService} from "../message/message.service";
import {ErrorHandler} from "../../classes/error-handler";

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable()
export class UserService {
  private findAllUsersUrl: string = '/api/users';
  private findUserByIdUrl: string = '/api/users/userInfo';
  private saveUserUrl:string = "/api/users";

  constructor(private http: HttpClient,
              private messageService: MessageService,
              private errorHandler: ErrorHandler) {}

  findAllUsers(): Observable<User[]> {
    this.messageService.add('UserService: żądanie findAllUsers wysłane pod adres:' + this.findAllUsersUrl);
    return this.http.get<User[]>(this.findAllUsersUrl)
      .pipe(
        tap(users => this.errorHandler.log('Przetworzono listę użytkowników')),
        catchError((error: any) => Observable.throw(this.errorHandler.handleError(error)))
      );
  }

  findUserById(id: number): Observable<User> {
    this.messageService.add(`UserService: żądanie findUserById dla id=${id}`);
    return this.http.get<User>(this.findUserByIdUrl, {
      params: {
        id: JSON.stringify(id)
      }
    })
      .pipe(
        tap(user => this.errorHandler.log(`Przetworzony użytkownik o id=${id}`)),
        catchError((error: any) => Observable.throw(this.errorHandler.handleError(error)))
      );
  }

  saveUser(user: User): Observable<Response> {
    this.messageService.add(`UserService: żądanie saveUser dla email ` + user.email);
    return this.http.post<Response>(this.saveUserUrl, {
      email: user.email,
      password: user.password,
      dateOfBirth: user.dateOfBirth,
      gender: user.gender
    }, httpOptions)
      .pipe(
        tap(user => this.errorHandler.log(`Zapisano użytkownika`)),
        catchError((error: any) => Observable.throw(this.errorHandler.handleError(error)))
      );
  }

}
