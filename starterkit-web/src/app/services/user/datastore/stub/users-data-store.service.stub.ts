import {Observable} from "rxjs";
import {User} from "../../../../classes/user";

export class UsersDataStoreServiceStub {
  private _emptyUser: User = new User;

  findUserById(userId: number): Observable<User> {
    return Observable.of(this._emptyUser);
  }

  loadAllUsers(): void {}
}
