import {Injectable} from '@angular/core';
import {UserService} from "../user.service";
import {User} from "../../../classes/user";
import {Observable} from "rxjs/Observable";

@Injectable()
export class UsersDataStoreService {

  private _allUsers: User[] = [];
  private _wasStoreLoaded: boolean = false;

  constructor(private userService: UserService) {
  }

  get allUsers(): User[] {
    return this._allUsers;
  }

  set allUsers(value: User[]) {
    this._allUsers = value;
  }

  findUserById(userId: number): Observable<User> {
    let foundUser: User = null;
    if (this._allUsers.length !== 0) {
      this._allUsers.some(function (user) {
        if (user.id === userId) {
          foundUser = user;
          return true;
        }
      });
      return this.createObservableForUser(foundUser);
    } else {
      return this.userService.findUserById(userId).flatMap(user => {
        this._allUsers.push(user); //TODO przemyśleć czy jest nie jest to przekombinowane rozwiązanie
        return this.createObservableForUser(user);
      });
    }
  }

  private createObservableForUser(user: User): Observable<User> {
    return new Observable(observer => {
      observer.next(user);
      observer.complete();
    });
  }

  loadAllUsers(): void {
    if (!this._wasStoreLoaded) {
      this.userService.findAllUsers()
        .subscribe(users => this._allUsers = users);
      this._wasStoreLoaded = true;
    }
  }

  reloadAllUsers(): void {
    this.userService.findAllUsers()
      .subscribe(users => this._allUsers = users);
  }

}
