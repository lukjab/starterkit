import {inject, TestBed} from '@angular/core/testing';

import {UsersDataStoreService} from './users-data-store.service';
import {UserService} from "../user.service";
import {UserServiceStub} from "../stub/user.service.stub";

describe('UsersDataStoreService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UsersDataStoreService,
        {provide: UserService, useClass: UserServiceStub}
      ]
    });
  });

  it('should be created', inject([UsersDataStoreService], (service: UsersDataStoreService) => {
    expect(service).toBeTruthy();
  }));
});
