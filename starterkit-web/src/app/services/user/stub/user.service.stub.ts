import {User} from "../../../classes/user";
import {Observable} from "rxjs";

export class UserServiceStub {
  private _emptyUser: User = new User;

  findAllUsers(): Observable<User[]> {
    return Observable.of([this._emptyUser]);
  }

  findUserById(id: number): Observable<User> {
    return Observable.of(this._emptyUser);
  }
}
