import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {MessageService} from "../message/message.service";
import {ErrorHandler} from "../../classes/error-handler";
import {Observable} from "rxjs/Rx";
import {catchError, tap} from "rxjs/operators";
import {Movie} from "../../classes/movie";
import {Rental} from "../../classes/rental";

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable()
export class RentalService {

  private rentMovieUrl: string = "/api/rentals";
  private findRentedMoviesForUserUrl: string = '/api/rentals/user';
  private returnRentalsForUserUrl: string = '/api/rentals/return';

  constructor(private http: HttpClient,
              private messageService: MessageService,
              private errorHandler: ErrorHandler) {
  }

  rentMovie(userId: number, movieId: number): Observable<Response> {
    this.messageService.add('RentalService: żądanie rentMovie wysłane pod adres ' + this.rentMovieUrl);
    return this.http.post<Response>(this.rentMovieUrl, {
      userId: userId,
      movieId: movieId
    }, httpOptions)
      .pipe(
        catchError((error: any) => Observable.throw(this.errorHandler.handleError(error)))
      );
  }

  findRentalsByUser(userId: number): Observable<Rental[]> {
    this.messageService.add('RentalService: żądanie findRentalsByUser wysłane pod adres ' + this.findRentedMoviesForUserUrl);
    return this.http.get<Movie[]>(this.findRentedMoviesForUserUrl, {
      params: {
        userId: JSON.stringify(userId)
      }
    }).pipe(
      tap(movie => this.errorHandler.log(`Przetworzono listę wypożyczonych filmów dla użytkownika o id ${userId}`)),
      catchError((error: any) => Observable.throw(this.errorHandler.handleError(error)))
    );
  }

  returnRentals(rentalsToReturn: string[]): Observable<Response> {
    this.messageService.add(`RentalServcie: żądanie returnRentals dla rental id: ${rentalsToReturn} wysłane pod adres ` + this.returnRentalsForUserUrl);
    return this.http.post<Response>(this.returnRentalsForUserUrl, {
      rentalsToReturn: rentalsToReturn
    }, httpOptions).pipe(
      catchError((error: any) => Observable.throw(this.errorHandler.handleError(error)))
    );
  }

}
