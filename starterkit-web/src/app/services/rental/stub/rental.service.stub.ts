import {Observable} from "rxjs";
import {Rental} from "../../../classes/rental";

export class RentalServiceStub {

  rentMovie(userId: number, movieId: number): Observable<Response> {
    const emptyResponse = new Response;
    return Observable.of(emptyResponse);
  }

  findRentalsByUser(userId: number): Observable<Rental[]> {
    const emptyRental = new Rental;
    return Observable.of([emptyRental]);
  }
}
