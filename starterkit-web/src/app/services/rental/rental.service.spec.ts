import {TestBed, inject} from "@angular/core/testing";
import {RentalService} from "./rental.service";
import {HttpClient, HttpHandler} from "@angular/common/http";
import {MessageService} from "../message/message.service";
import {ErrorHandler} from "../../classes/error-handler";

describe('RentalService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RentalService, HttpClient, HttpHandler, MessageService, ErrorHandler]
    });
  });

  it('should be created', inject([RentalService], (service: RentalService) => {
    expect(service).toBeTruthy();
  }));
});
