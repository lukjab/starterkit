import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {UsersComponent} from "../../components/users/users/users.component";
import {DashboardComponent} from "../../components/dashboard/dashboard.component";
import {UserDetailComponent} from "../../components/users/user-detail/user-detail.component";
import {SearchMoviesComponent} from "../../components/searchmovies/searchmovies.component";
import {MovieDetailComponent} from "../../components/movie-detail/movie-detail.component";
import {AddUserComponent} from "../../components/users/add-user/add-user.component";
import {UserPanelComponent} from "../../components/users/user-panel/user-panel.component";
import {ManageUserPanelComponent} from "../../components/users/manage-user-panel/manage-user-panel.component";

const routes: Routes = [
  {path: '', redirectTo: '/dashboard', pathMatch: 'full'},
  {path: 'dashboard', component: DashboardComponent},
  {path: 'user-detail/:id', component: UserDetailComponent},
  {path: 'movie-detail/:id', component: MovieDetailComponent},
  {path: 'users', component: UsersComponent},
  {path: 'searchmovies', component: SearchMoviesComponent},
  {path: 'add-user', component: AddUserComponent},
  {path: 'user-panel', component: UserPanelComponent},
  {path: 'manage-user-panel/:id', component: ManageUserPanelComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
